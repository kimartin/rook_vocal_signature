import os

# suppress INFO lines when importing Tensorflow (leaves WARNING and ERROR lines)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'


# Working directory
os.chdir(path="C:/Users/killian/Desktop/database_rooks/clustering/signatures")
# os.chdir(path="C:/Users/killian/Desktop/database_rooks/clustering/signatures2023")

# Path to the data
label_path = 'vocs'
# audio_path = 'audio'
audio_path = "//10.33.136.29/cog/Vocalisation/2022_LouiseEmeline/audio2022"
data_path = None
cluster_data = None


# File formats
label_format = "tsv"
audio_format = "wav"
data_format = "pickle"

# Output file names
voc_name = "Voc"
noise_name = "Background"
excluded_name = "Overlap"

# Parameters
# general audio
sample_rate = 48000
bit = 16

# filter settings
btype = "high"
fmin = 100
fmax = 24000

# spectrogram paramaters
wl = .01  # window length is in seconds
window_fn = "hamming"
ovlp = 80  # in %
extension_factor = 2.
power = 2.
use_tensorflow = callable(window_fn)

# denoising
pre = 1.
n_grad_freq = 4
n_grad_time = 4
q = .95
# std=1.6
prop_decrease = 1.


# parameters for considering eligible noise intervals
# shortest noise eligible
minimum_noise = 0.
# longest interval between vocalisations to be denoised with the same noise sample
maximum_interval = 30.
# collect noise until as many seconds as dictated by this argument before before and after
maximum_noise_extend = 30.

# Mel-spectrogram args
n_fft = int(round(extension_factor * wl * sample_rate))
n_mels = 80
fmin = fmin
fmax = fmax
htk = False
norm = 'slaney'
sr = sample_rate

# Saturation tolerance
# excludes channels with over saturation_tolerance[0] consecutive samples at saturation, or with over
# saturation_tolerance[1]*100 % total samples at saturation
saturation_tolerance = (50, .1)


# Gather all the variables in this script within a dict for easy access
arg_dict = {k: v for k, v in locals().items() if not k.startswith('__')}
