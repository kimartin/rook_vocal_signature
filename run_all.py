from subprocess import run
from datetime import datetime
import os

# Runs all the scripts in the files list
# Each script is run only after the previous one is finished
os.chdir("C:/Users/killian/PycharmProjects/vocal_signature")
path_to_scripts = 'scripts'
files = [
    'dataset',
    'spectrogramming',
    'distance_matrices',
    # 'choose_umap_parameters'
    # 'cluster',
    # 'knn_stats'
]

print(datetime.now())
for program in files:
    run(f'python -m {path_to_scripts}.{program}', shell=True)
    print(f'Finished running {program} at: {datetime.now()}')

# run('SHUTDOWN /S /F /T 1', shell=True)