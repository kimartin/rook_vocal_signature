import warnings

import numpy as np
import scipy.spatial
from numba import njit, prange
import shutil
from joblib import Parallel, delayed, dump, load
import os
from math import ceil
import gc
from functools import partial
# from functions.wav_manipulation import frame
from scipy.spatial.distance import squareform


@njit(nogil=True, cache=True)
def inf_to_0(arr):
    return np.where(np.isfinite(arr), arr, 0.)


@njit(nogil=True, cache=True)
def manhattan_unit(a, b):
    return np.sum(np.abs(a-b))


@njit(nogil=True, cache=True)
def euclidean_unit(a, b):
    return np.linalg.norm(a - b)


@njit(nogil=True, cache=True)
def sqeuclidean_unit(a, b):
    res = a - b
    return res @ res


@njit(nogil=True, cache=True)
def cosine_unit(a, b):
    a = a.ravel()
    b = b.ravel()
    return 1. - a @ b / np.sqrt((a @ a) * (b @ b))


@njit(nogil=True, cache=True)
def sqeuclidean(a, b):
    res = np.dot(a, b.T)
    res *= -2
    res += np.expand_dims(np.sum(a**2, axis=-1), axis=1)
    res += np.expand_dims(np.sum(b**2, axis=-1), axis=0)
    return np.abs(res)


@njit(nogil=True, cache=True)
def euclidean(a, b):
    res = np.dot(a, b.T)
    res *= -2
    res += np.expand_dims(np.sum(a**2, axis=-1), axis=1)
    res += np.expand_dims(np.sum(b**2, axis=-1), axis=0)
    return np.sqrt(np.abs(res))


@njit(nogil=True, cache=True)
def normeuclidean(a, b):
    res = np.dot(a, b.T)
    res *= -2
    res += np.expand_dims(np.sum(a**2, axis=-1), axis=1)
    res += np.expand_dims(np.sum(b**2, axis=-1), axis=0)
    return np.sqrt(np.abs(res)) * a.shape[-1] ** -.5


@njit(nogil=True, cache=True)
def cosine_similarity(a, b):
    res = np.dot(a, b.T)
    a = np.sum(a**2, axis=-1)
    a[a > 0] **= -.5
    b = np.sum(b**2, axis=-1)
    b[b > 0] **= -.5
    res *= np.expand_dims(a, axis=1)
    res *= np.expand_dims(b, axis=0)
    return res


@njit(nogil=True, cache=True)
def cosine(a, b):
    return np.abs(1. - cosine_similarity(a, b))


@njit(nogil=True, cache=True)
def angular(a, b):
    return np.arccos(np.clip(cosine_similarity(a, b), a_min=-1, a_max=1)) / np.pi


@njit(nogil=True, cache=True)
def accumulated_cost_matrix(dist_matrix, weight=1.):
    N, M = dist_matrix.shape
    accumulated = np.full((N+1, M+1), fill_value=np.inf, dtype=dist_matrix.dtype)
    accumulated[0, 0] = 0
    for i in prange(1, N+1):
        for j in prange(1, M+1):
            val = dist_matrix[i - 1, j - 1]
            accumulated[i, j] = min(val + accumulated[i-1, j],
                                    weight * val + accumulated[i-1, j-1],
                                    val + accumulated[i, j-1])

    return accumulated


@njit(nogil=True, cache=True)
def accumulated_distance(dist_matrix, weight=1.):
    N, M = dist_matrix.shape
    prev_row = np.full(shape=M+1, fill_value=np.inf)
    prev_row[0] = 0.

    for i in prange(1, N+1):
        current_row = np.full(shape=M+1, fill_value=np.inf)
        for j in prange(1, M+1):
            val = dist_matrix[i - 1, j - 1]
            current_row[j] = min(weight * val + prev_row[j-1],
                                 val + prev_row[j],
                                 val + current_row[j-1])
        prev_row = current_row

    return current_row[-1]


@njit(nogil=True, cache=True)
def dtw(a, b, method=euclidean, weight=1.):
    """
    Computes DTW between two samples a and b.
    """
    dist_matrix = method(a, b)
    dtw_distance = accumulated_distance(dist_matrix, weight=weight)
    return dtw_distance


@njit(nogil=True, cache=True)
def ddtw(a, b, method=euclidean, weight=1.):
    a = (a[2:] + 2*a[1:-1] - 3*a[:-2]) / 4
    b = (b[2:] + 2*b[1:-1] - 3*b[:-2]) / 4

    dist_matrix = method(a, b)
    dtw_distance = accumulated_distance(dist_matrix, weight=weight)

    return dtw_distance


@njit(nogil=True, cache=True)
def warping_path(D):
    """Compute the warping path given an accumulated cost matrix

    Notebook: C3/C3S2_DTWbasic.ipynb

    Args:
        D (np.ndarray): Accumulated cost matrix

    Returns:
        P (np.ndarray): Optimal warping path
    """
    n = D.shape[0] - 1
    m = D.shape[1] - 1
    P = np.zeros((2, n+m), dtype=np.int32)
    k = n + m - 1
    P[0, k] = n
    P[1, k] = m
    while n > 0 and m > 0:
        v0, v1, v2 = D[n - 1, m - 1], D[n - 1, m], D[n, m - 1]
        if v0 <= v1 and v0 <= v2:
            n -= 1
            m -= 1
        elif v1 <= v0 and v1 <= v2:
            n -= 1
        else:
            m -= 1
        k -= 1
        P[0, k] = n
        P[1, k] = m
    P -= 1
    return P[0, k+1:], P[1, k+1:]


@njit(nogil=True, cache=True)
def apply_along_axis_0(func1d, arr):
    """Like calling func1d(arr, axis=0)"""
    if arr.size == 0:
        raise RuntimeError("Must have arr.size > 0")
    ndim = arr.ndim
    if ndim == 0:
        raise RuntimeError("Must have ndim > 0")
    elif 1 == ndim:
        return func1d(arr)
    else:
        result_shape = arr.shape[1:]
        out = np.empty(result_shape, arr.dtype)
        _apply_along_axis_0(func1d, arr, out)
        return out


@njit(nogil=True, cache=True)
def _apply_along_axis_0(func1d, arr, out):
    """Like calling func1d(arr, axis=0, out=out). Require arr to be 2d or bigger."""
    ndim = arr.ndim
    if ndim < 2:
        raise RuntimeError("_apply_along_axis_0 requires 2d array or bigger")
    elif ndim == 2:  # 2-dimensional case
        for i in prange(len(out)):
            out[i] = func1d(arr[:, i])
    else:  # higher dimensional case
        for i, out_slice in enumerate(out):
            _apply_along_axis_0(func1d, arr[:, i], out_slice)


@njit
def nb_mean_axis_0(arr):
    return apply_along_axis_0(np.mean, arr)


@njit
def nb_sum_axis_0(arr):
    return apply_along_axis_0(np.sum, arr)


@njit
def norm2(arr):
    return np.sqrt(np.sum(arr**2))


@njit
def nb_norm2_axis_0(arr):
    return apply_along_axis_0(norm2, arr)


@njit(nogil=True, cache=True)
def nb_max_axis_0(arr):
    return apply_along_axis_0(np.max, arr)


@njit(nogil=True, cache=True)
def nb_norm_axis_0(arr):
    return apply_along_axis_0(np.linalg.norm, arr)


@njit(nogil=True, cache=True)
def dfw(a, b, method=euclidean, weight=1.):
    """
    Dynamic Frequency Warping.
    Essentially:
        - compute DTW of a and b
        - stretch both according to the warping path obtaining: stretch_a and stretch_b
        - compute DTW on the rows of stretch_a and stretch_b
    """

    # First DTW on time axis (first axis)
    dist_matrix = method(a, b)
    dtw_matrix = accumulated_cost_matrix(dist_matrix, weight=weight)

    # Time alignment
    warp_a, warp_b = warping_path(dtw_matrix)

    # Second DTW on frequency axis
    # keep a and b intact
    dist_matrix = method(np.ascontiguousarray(a[warp_a, :].T), np.ascontiguousarray(b[warp_b, :].T))
    dtw_matrix = accumulated_cost_matrix(dist_matrix, weight=weight)

    # Frequency alignment
    warp_a, warp_b = warping_path(dtw_matrix)

    # Final DTW
    dist_matrix = method(np.ascontiguousarray(a[:, warp_a]), np.ascontiguousarray(b[:, warp_b]))
    dist = accumulated_distance(dist_matrix, weight=weight)

    return dist


# @njit(nogil=True, cache=True)
# def independent_dftw(a, b, weight=1.):
#     """
#     Dynamic Frequency-Time Warping with independent frequency warping between successive time frames.
#     Essentially, it's the DTW with DTW itself as the method argument.
#
#     NB: Currently EXTREMELY time-intensive, do not use!
#
#     a and b should be arrays of shape [x1, y], [x2, y]
#     """
#     N, M = len(a), len(b)
#
#     distance = euclidean_matrix(a, b)
#     dist_matrix = np.empty(shape=(N, M))
#     for i in prange(N):
#         for j in prange(M):
#             dist_matrix[i, j] = accumulated_distance(distance[i, ..., j], weight=weight)
#     dtw_distance = accumulated_distance(dist_matrix, weight=weight)
#
#     return dtw_distance


def make_callable(method):
    if method == 'euclidean':
        @njit(nogil=True, cache=True)
        def method(a, b, ssq_a, ssq_b):
            res = np.dot(a, b.T)
            res *= -2
            res += np.expand_dims(ssq_a, axis=1)
            res += np.expand_dims(ssq_b, axis=0)
            return np.sqrt(np.abs(res))

    elif method == 'normeuclidean':
        @njit(nogil=True, cache=True)
        def method(a, b, ssq_a, ssq_b):
            res = np.dot(a, b.T)
            res *= -2
            res += np.expand_dims(ssq_a, axis=1)
            res += np.expand_dims(ssq_b, axis=0)
            return np.sqrt(np.abs(res)) / a.shape[-1]

    elif method == 'cosine':
        @njit(nogil=True, cache=True)
        def method(a, b, ssq_a, ssq_b):
            res = np.dot(a, b.T)
            res *= np.expand_dims(ssq_a, axis=1)
            res *= np.expand_dims(ssq_b, axis=0)
            return np.abs(1 - res)

    elif method == 'angular':
        @njit(nogil=True, cache=True)
        def method(a, b, ssq_a, ssq_b):
            res = np.dot(a, b.T)
            res *= np.expand_dims(ssq_a, axis=1)
            res *= np.expand_dims(ssq_b, axis=0)
            return np.arccos(np.clip(res, a_min=-1, a_max=1)) / np.pi

    return method


def dtw_distance_matrix(x,
                        dtw_method=dtw,
                        method=euclidean,
                        n_jobs=-1,
                        batch_size=-1,
                        pre_dispatch='2 * n_jobs',
                        verbose=10,
                        normalize='joint',
                        # twi=False,
                        memmap_folder='./joblib_memmap',
                        shape=1,
                        filename=None,
                        dtw_method_kwargs={'weight': 1.},
                        condensed=True,
                        ):
    """
    Takes a list or array x and compute the pairwise distance matrix between all elements of x.
    Note that len(x) should correspond to the number of samples (so if x is an array, this is x.shape[0]).

    LEAVING OUT FOR NOW
    # Optionally provide a second object y with the same conditions as x. If y is provided, the distance matrix will be
    # computed between all elements of x and all elements of y.
    """
    try:
        os.mkdir(memmap_folder)
    except FileExistsError:
        pass

    assert normalize in [False, 'joint', 'separate'], 'normalize must be one of: ' \
                                                      'False (no normalization), ' \
                                                      '"separate" (each element is normalised separately),' \
                                                      'or "joint" (joint normalization of element pairs)'

    # best performance is achieved for a common shape[0] for all elements of x, so we either make sure it's the case,
    # or that it can be achieved by transposing
    shape0, shape1 = zip(*[_.shape for _ in x])
    set0 = len(set(shape0))
    set1 = len(set(shape1))
    if set0 > 1 and set1 > 1:
        raise ValueError('No common axis size in the elements of x, make sure all elements of x have the same '
                         'size in either axis 0 (accepted, but will be flipped) or axis 1 (best).')
    elif set0 == 1 and set1 == 1:
        print('Both axes are of the same size in all elements of x. Axis 1 will be assumed to be the frequency '
              'axis, make sure this is the case.')
    elif set0 == 1:
        print('Axis 0 has the same size in all elements of x. Axis 1 is assumed to be the frequency axis and '
              'thus should be of the same size for the later functions. Transposing all elements of x...')
        x = [np.ascontiguousarray(_.T) for _ in x]

    # if shape > 1:
    #     with Parallel(n_jobs=-1, verbose=0) as par:
    #         # axis = 0 is the frequency axis, so we concatenate slices of the spectrogram with this
    #         x = par(delayed(frame)(_, shape, axis=0) for _ in x)

    # preparing constants for runtime
    len_x = len(x)
    n_iter = (len_x * (len_x - 1)) // 2
    output_shape = (len_x, len_x)
    if batch_size < 0:
        if n_jobs < 0:
            n_jobs += os.cpu_count() + 1
        quadratic_batch_size = ceil(n_iter / n_jobs)
        pre_dispatch = 'all'
    else:
        quadratic_batch_size = batch_size

    # if twi:
    #     x = [_[np.unique(_, axis=0, return_index=True)[1]] for _ in x]

    if normalize == 'separate':
        def norm(a):
            return (a - np.mean(a)) / np.std(a)

        with Parallel(n_jobs=-1, verbose=0) as par:
            x = par(delayed(norm)(_) for _ in x)

    def make_memmap(arr, memmap_path, name):
        memmap_filename = f'{memmap_path}/{name}'
        dump(value=arr, filename=memmap_filename)
        arr = load(memmap_filename, mmap_mode='r')
        return arr

    idx = np.triu_indices(n=len_x, k=1)
    # flat_idx = (len_x * (len_x - 1) // 2) - (len_x - idx[0]) * ((len_x - idx[0]) - 1) // 2 + idx[1] - idx[0] - 1
    # np.arange(n_iter) is exactly the same as flat_idx here
    idx = np.stack([*idx, np.arange(n_iter)], axis=1).astype('int')

    # idx = np.array(np.triu_indices(n=len_x, k=1)).T
    # idx = make_memmap(idx, memmap_path=memmap_folder, name='idx_memmap')
    x = make_memmap(x, memmap_path=memmap_folder, name='x_memmap')

    # output = np.memmap(f'{memmap_folder}/output_memmap', dtype=x[0].dtype, shape=output_shape, mode='w+')
    output = np.memmap(f'{memmap_folder}/output_memmap', dtype=x[0].dtype, shape=(n_iter, ), mode='w+')

    def build_func(dtw_method, method, norm, **kwargs):
        if dtw_method:
            distance_func = partial(dtw_method, method=method, **kwargs)
        else:
            distance_func = method

        # build the actual function here to avoid needing to pass the same arguments in the delayed function
        if norm == 'joint':
            @delayed
            def func(ijk, x, joint_mean, joint_std, output):
                i, j, k = ijk
                res = distance_func(
                    (x[i] - joint_mean[k]) / joint_std[k],
                    (x[j] - joint_mean[k]) / joint_std[k]
                )
                output[k] = res
                # output[i, j] = output[j, i] = res
        else:
            @delayed
            def func(ijk, x, output):
                i, j, k = ijk
                res = distance_func(x[i], x[j])
                output[k] = res
                # output[i, j] = output[j, i] = res

        return func

    if dtw_method_kwargs is None:
        dtw_method_kwargs = dict()

    dtw_func = build_func(dtw_method=dtw_method, method=method, norm=normalize, **dtw_method_kwargs)

    parallel_args = dict(n_jobs=n_jobs,
                         batch_size=quadratic_batch_size,
                         verbose=verbose,
                         pre_dispatch=pre_dispatch)

    if normalize == 'joint':
        # Prepare memmaps containing the joint means and standard deviation for each pair of samples
        sum_x = np.array([np.sum(_) for _ in x])
        ssq_x = np.array([np.sum(_ * _) for _ in x])
        sizes_x = np.array([np.size(_) for _ in x])

        joint_sizes_matrix = np.expand_dims(sizes_x, axis=0) + np.expand_dims(sizes_x, axis=1)

        joint_means_matrix = np.expand_dims(sum_x, axis=0) + np.expand_dims(sum_x, axis=1)
        joint_means_matrix /= joint_sizes_matrix

        joint_std_matrix = np.expand_dims(ssq_x, axis=0) + np.expand_dims(ssq_x, axis=1)
        joint_std_matrix -= joint_sizes_matrix * (joint_means_matrix ** 2)
        np.abs(joint_std_matrix, out=joint_std_matrix)
        joint_std_matrix = np.sqrt(joint_std_matrix / (joint_sizes_matrix - 1))

        del joint_sizes_matrix

        joint_means_matrix = make_memmap(squareform(joint_means_matrix, checks=False), memmap_path=memmap_folder, name='means_memmap')
        joint_std_matrix = make_memmap(squareform(joint_std_matrix, checks=False), memmap_path=memmap_folder, name='std_memmap')

        # Actual loop
        Parallel(**parallel_args)(dtw_func(ijk, x, joint_means_matrix, joint_std_matrix, output) for ijk in idx)

        joint_means_matrix._mmap.close()
        joint_std_matrix._mmap.close()
        del joint_std_matrix, joint_means_matrix
    else:
        # Actual loop
        Parallel(**parallel_args)(dtw_func(ijk, x, method, output) for ijk in idx)

    if filename is not None:
        if condensed:
            dump(output, open(filename, 'wb'))
        else:
            dump(squareform(output.copy()), open(filename, 'wb'))
        dtw_matrix = None
    else:
        dtw_matrix = output.copy()
        if not condensed:
            dtw_matrix = squareform(dtw_matrix)

    # Ugly hack to remove all the memmap files
    # idx._mmap.close()
    # output._mmap.close()
    # x in particular since it's a list in the current code, so we close each element of it separately
    [_._mmap.close() for _ in x]
    del x, output, idx
    gc.collect()
    os.remove(f'{memmap_folder}/means_memmap')
    os.remove(f'{memmap_folder}/std_memmap')
    # os.remove(f'{memmap_folder}/idx_memmap')
    os.remove(f'{memmap_folder}/x_memmap')
    os.remove(f'{memmap_folder}/output_memmap')
    shutil.rmtree(memmap_folder)

    return dtw_matrix


def complexity_estimate(a, b, method=euclidean):
    ca = np.sum(np.diagonal(method(a[1:], a[:-1])))
    cb = np.sum(np.diagonal(method(b[1:], b[:-1])))
    return max(ca, cb) / min(ca, cb)


def cid_distance_matrix(x,
                        method=euclidean_unit,
                        normalize=True,
                        robust=False,
                        # twi=False,
                        n_jobs=1,
                        ):
    # if twi:
    #     def compress(arr, axis=0):
    #         return arr[np.unique(arr, axis=axis, return_index=True)[1]]
    #
    #     x = [compress(_) for _ in x]

    if normalize:
        if robust:
            def normalize_func(a):
                return (a - np.median(a)) / (np.quantile(a, q=.75) - np.quantile(a, q=.25))
        else:
            def normalize_func(a):
                return (a - np.mean(a)) / np.std(a)

        with Parallel(n_jobs=n_jobs,
                      pre_dispatch='all'
                      ) as p:
            x = p(delayed(normalize_func)(_) for _ in x)

    def inner(a, b):
        return np.sum(np.diagonal(method(a, b)))

    with Parallel(n_jobs=n_jobs,
                  pre_dispatch='all'
                  ) as p:
        cid = p(delayed(inner)(x_[:-1], x_[1:]) for x_ in x)

    cid = np.reshape(cid, (-1, 1))
    cid = np.maximum(cid, cid.T) / np.minimum(cid, cid.T)

    return cid


def length_matrix(x, type='sum', axis=-1, condensed=True):
    """
    Computes a normalization matrix for DTW.
    Each element of the matrix is given by Pythagora's formula applied to the lengths of all pairs of elements of x:

        matrix[i, j] = np.sqrt(len(x[i])**2 + len(x[j])**2)
    """
    out = [_.shape[axis] for _ in x]
    out = np.reshape(out, (-1, 1))

    if type == 'sum':
        out = out + out.T
    elif type == 'maximum':
        out = np.maximum(out, out.T)
    elif type == 'ratio':
        out = np.maximum(out, out.T) / np.minimum(out, out.T)

    if condensed:
        out = squareform(out, checks=False)

    return out


if __name__ == '__main__':
    from time import time
    import matplotlib.pyplot as plt

    x = np.random.uniform(size=(500, 40))
    ls0 = np.random.choice(range(100, 600, 10), size=1000, replace=True)
    ls = [np.random.uniform(size=(_, 80)) for _ in ls0]
    dtw_distance_matrix(ls, order=False)
    ls = [np.random.uniform(size=(_, 80)) for _ in ls0]
    dtw_distance_matrix(ls, order=True)
    ls = [np.random.uniform(size=(_, 80)) for _ in ls0]
    dtw_distance_matrix(ls, order=False)
    ls = [np.random.uniform(size=(_, 80)) for _ in ls0]
    dtw_distance_matrix(ls, order=True)


    dtw(x, x)
    independent_dftw(x, x)

    te = time()
    [dtw(x, x) for _ in range(10)]
    t1 = time() - te

    te = time()
    [independent_dftw(x, x) for _ in range(10)]
    t2 = time() - te

    print(t1, t2)

    # plt.scatter(lengths, np.log10(times1))
    # plt.scatter(lengths, np.log10(times2))
    # plt.show()


