import pandas as pd
import numpy as np
# import tensorflow as tf
import os
import re
from tqdm import tqdm
from operator import xor
from functools import reduce, partial
import itertools
import contextlib
import joblib
from scipy import ndimage
import inspect
from typing import Union
import scipy.stats


def tqdm2(x, **kwargs):
    """
    Thin wrapper around tqdm to automatically add total argument
    """
    if not hasattr(x, "len"):
        x = list(x)
    return tqdm(x, total=len(x), position=0, leave=True)


@contextlib.contextmanager
def tqdm_joblib_wrapper(tqdm_object):
    """Context manager to patch joblib to report into tqdm progress bar given as argument"""

    class TqdmBatchCompletionCallback(joblib.parallel.BatchCompletionCallBack):
        def __call__(self, *args, **kwargs):
            tqdm_object.update(n=self.batch_size)
            return super().__call__(*args, **kwargs)

    old_batch_callback = joblib.parallel.BatchCompletionCallBack
    joblib.parallel.BatchCompletionCallBack = TqdmBatchCompletionCallback
    try:
        yield tqdm_object
    finally:
        joblib.parallel.BatchCompletionCallBack = old_batch_callback
        tqdm_object.close()


def tqdm_joblib(**kwargs):
    return tqdm_joblib_wrapper(tqdm(**kwargs))


def flatten(ll: list):
    return [item for sub in ll for item in sub]


def onsets_offsets(arr: np.ndarray):
    elements, nelements = ndimage.label(arr)
    if nelements == 0:
        return np.array([[0], [0]])
    onsets, offsets = np.array(
        [
            np.where(elements == element)[0][np.array([0, -1])] + np.array([0, 1])
            for element in np.unique(elements)
            if element != 0
        ]
    ).T
    # remove length-0 segments
    onsets, offsets = zip(*[[on, off] for on, off in zip(onsets, offsets) if off > on])
    return np.array([onsets, offsets])


def frame(x, w, axis=0):
    """
    x must be a 2D array for this, no current support for 1D and >2D arrays.
    """
    if axis == 0:
        x = np.ascontiguousarray(x.T)

    stride = x.shape[-1]
    L = stride * w

    out = np.zeros(shape=(x.shape[0] + 2 * (w // 2), *x.shape[1:]))
    out[slice(w // 2, len(x) + w // 2)] = x
    out = out.reshape(-1)

    nrows = ((out.size - L) // stride) + 1
    n = out.strides[0]
    x = np.lib.stride_tricks.as_strided(out, shape=(nrows, L), strides=(stride * n, n))

    if axis == 0:
        x = x.T

    return np.ascontiguousarray(x)


def broadcastable(a: np.ndarray, b: np.ndarray):
    """
    Checks if two arrays a and b are broadcastable.

    Basically:
        - a and b should have the same size or 1 in all axes
        - if a.ndim < b.ndim,
    """
    return all((m == n) or (m == 1) or (n == 1) for m, n in zip(a.shape[::-1], b.shape[::-1]))


def product_dict(**kwargs):
    """
    Gets all combinations from the given named arguments. Each combination is returned as dict with keys given by the
    names of the arguments, and values as one value from each of the lists in the arguments

    Exemple:
    list(product_dict(a = ['A', 'B'], b = [3, 4]))
    >>> [{'a': 'A', 'b': 3}, {'a': 'A', 'b': 4}, {'a': 'B', 'b': 3}, {'a': 'B', 'b': 4}]

    :param kwargs: arbitrary named arguments. Each element should be a list, even if only of length 1. The elements of
    each list can be of any number of different types.
    :return: generator, containing all possible combinations of the dicts in kwargs
    """
    keys = kwargs.keys()
    vals = kwargs.values()
    for instance in itertools.product(*vals):
        yield dict(zip(keys, instance))


def arg_names(function):
    """
    Gets a list of all the named arguments accepted by the input function.
    """
    return list(inspect.signature(function).parameters.keys())


def restrict_kwargs(func, d, **kwargs):
    """
    Gets only the subset of dict d with keys within the arguments of func.
    """
    arguments = arg_names(func)
    new_d = {k: d[k] for k in d if k in arguments}
    new_kwargs = {k: kwargs[k] for k in kwargs if k in arguments}
    new_d.update(new_kwargs)
    return new_d


def compose(*args):
    """
    Composes a list of functions from left to right
    """
    if len(args) == 1:
        return args[0]

    def unit_compose(f, g):
        return lambda x: f(g(x))

    return reduce(unit_compose, reversed(args), lambda x: x)


def listfiles(path,
              pattern=None,
              negate=False,
              full_names=False,
              ):
    files = sorted(os.listdir(path))

    if pattern is None:
        return files

    if not isinstance(pattern, list):
        pattern = [pattern]
    if not isinstance(negate, list):
        negate = [negate] * len(pattern)

    for pat, neg in zip(pattern, negate):
        files = [f for f in files if xor(bool(re.search(pat, f)), neg)]

    if full_names and path is not None:
        files = [f'{path}/{f}' for f in files]

    return files


def find_overlaps(x,
                  start="Start",
                  end="End",
                  separated_by=0.,
                  name='group',
                  by=None,
                  allow_shared_boundaries=True):
    """
    Finds the overlaps in ranges defined by the columns "start" and "end" of pd.DataFrame object x.
    This includes overlaps that include multiple rows at once.

    Example:
    x = pd.DataFrame({"Start": [0, 10, 20, 30, 40, 50, 60],
                      "End":   [9, 21, 28, 53, 42, 55, 70]
    find_overlaps(x)
            Start  End  group
                0    9      1
               10   21      2
               20   28      2
               30   53      3
               40   42      3
               50   55      3
               60   70      4

    :param x: a pd.DataFrame object
    :param start: str. Gives the column that defines the start of the range.
    :param end: str. Gives the column that defines the end of the range.
    :param separated_by: float. Allows extending (if > 0) or shrinking (if <0) intervals so that overlaps will be
    considered either more broadly or more stringently.
        separated_by > 0: overlaps will be considered even if the actual values are separated by up to its value.
            e.g. separated_by = 1: intervals [1, 2.0001] and [2.9999, 4] are STILL considered overlapping.
        separated_by < 0: overlaps will be considered only if the actual values really overlap by more than its value
            e.g. separated_by = -1: intervals [1, 2.9999] and [2.0001, 4] are NOT considered overlapping.
    :param allow_shared_boundaries: bool. If True (default), intervals that share one boundary are not counted as
        overlapping.
    :param by: str or list of str. Optional columns that define groups within x. Rows that overlap in the
        start and end columns, but have different group as defined by this argument, will not be considered overlapping.
    :param name: str. Name of the "group" column
    :return: x with addition "group" column. The values in this column correspond to overlaps.
    """

    # Flatten start and end into a single column
    startdf = pd.DataFrame({'time': x[start] - separated_by / 2, 'what': 1})
    enddf = pd.DataFrame({'time': x[end] + separated_by / 2, 'what': -1})

    # Sort values: "time" for chronological order, and "what" to avoid consider shared boundaries as overlap
    sort_cols = ['time']

    if allow_shared_boundaries:
        sort_cols += ['what']

    if by is not None:
        startdf = pd.concat([startdf, x[by]], axis=1)
        enddf = pd.concat([enddf, x[by]], axis=1)
        sort_cols = [*by, *sort_cols] if isinstance(by, list) else [by, *sort_cols]

    mergdf = pd.concat([startdf, enddf]).sort_values(sort_cols)

    # Count intervals currently going on over the entire column
    mergdf['running'] = mergdf['what'].cumsum()

    # Finds the starts where there is exactly one interval
    mergdf['newwin'] = mergdf['running'].eq(1) & mergdf['what'].eq(1)

    # Group
    mergdf[name] = mergdf['newwin'].cumsum()

    # Adds the group back to the original data
    x[name] = mergdf[name].loc[mergdf['what'].eq(1)] - 1

    if by is not None and x[by].drop_duplicates(keep='first').shape[0] > 1:
        # if there were subgroups, we refactorise to avoid weirdness with the new column values
        # (like values being out of order)
        # sort = False preserves the order of groups in x
        x[name] = flatten([(g[name] - g[name].min()) for _, g in x.groupby(by=by, sort=False)])

    return x


def merge_overlaps(x, start="Start", end="End", by=None, separated_by=0.):
    """
    Merges the overlaps found by find_overlaps.
    :param x, start, end: passed to find_overlaps
    :param by: None or list of columns of x.  Intended to define groups, where overlaps will be ignored. Currently
    ignored as it does not work as intended.
    :return:
    """
    # Merges the overlaps found by find_overlaps
    columns = [start, end]

    x = find_overlaps(x, start=start, end=end, separated_by=separated_by, by=by)

    dict_agg = {start: 'min', end: 'max'}

    if by is not None:
        if not isinstance(by, list):
            by = [by]
        dict_agg = dict(**{_: 'first' for _ in by}, **dict_agg)
        columns += by

    res = x.groupby(["group"]).agg(dict_agg)

    res = res.reset_index()[columns]
    return res


def drop_overlaps(x, start="Start", end="End", by=None, separated_by=0., return_excluded=False):
    """
    Finds and drop overlapping rows of a pd.DataFrame object.
    Overlaps are defined as ranges [start, end] that overlap, with optional by argument to define subsetting groups.

    separated_by allows control of what constitutes an overlap (rows separated by less than its value are still
    considered overlapping; if negative, rows that overlap by less than its absolute value are still not considered
    overlapping).

    return_excluded: if True, the dropped rows are returned as a separate pd.DataFrame object, and the output of
    the function is a tuple of two pd.DataFrame objects.
    """
    columns = list(x.columns)

    if not isinstance(by, list):
        by = [by]

    if by != [None]:
        x = pd.concat([merge_overlaps(x=group, start=start, end=end,
                                      separated_by=separated_by,
                                      by=[c for c in columns if c not in [start, end]])
                       for _, group in x.groupby(by)])

    x.sort_values(start, inplace=True)
    x.reset_index(drop=True, inplace=True)

    x = find_overlaps(x=x, start=start, end=end, separated_by=separated_by)

    new = x.drop_duplicates(subset=["group"], keep=False)
    new = new.reset_index()[columns]
    if return_excluded:
        excluded = x.loc[x.duplicated(subset=["group"], keep=False)]
        excluded = excluded.reset_index()[columns]
        return new, excluded
    else:
        return new


def find_closest(value, series, type="before"):
    """
    Gets the index corresponding to the row of df[colname] closest to value, conditioned by type
    :param value: float. The value to find the closest neighbour(s) to.
    :param df: A pandas dataframe
    :param colname: The name of a column of df. Values will be searched in this column
    :param type: One of "before", "after", or "both":
        - "before": returns ind such that df[colname].loc[ind] <= value and is the closest such value
        - "after": returns ind such that df[colname].loc[ind] >= value and is the closest such value
        - "both": returns the results of both "before" and "after
    :return:
    """
    if not type in ["before", "after", "both"]:
        raise ValueError("type must be one of 'before', 'after', or 'both'.")

    exactmatch = series[series == value]
    if exactmatch.empty:
        if type == "before":
            res = series[series < value]
            ind = res.idxmax() if not res.empty else None
        elif type == "after":
            res = series[series > value]
            ind = res.idxmin() if not res.empty else None
            if ind is None:
                print(value, series, res.shape[0])
        else:
            res_lower = series[series < value]
            res_higher = series[series > value]
            ind = [res_lower.idxmax() if not res_lower.empty else None,
                   res_higher.idxmin() if not res_higher.empty else None]
    else:
        ind = exactmatch.index.values[0]
    return ind


# def tf_log10(x):
#     return tf.math.log(x) / tf.math.log(tf.constant(10, x.dtype))


# def tf_dB(x, ref=None, axis=None, dB_range=None):
#     if callable(ref):
#         ref = ref(x)
#     elif ref == "max0":
#         ref = tf.reduce_max(x, axis=axis, keepdims=True)
#     elif ref is None:
#         ref = tf.constant(1, x.dtype)
#     db = 10 * tf_log10(tf.math.divide_no_nan(x, ref))
#     if dB_range is not None:
#         db = tf.clip_by_value(db + dB_range,
#                               clip_value_min=0,
#                               clip_value_max=dB_range) / dB_range
#     return db


def np_dB(x, ref=None, axis=None, dB_range=None):
    if callable(ref):
        ref = ref(x, axis=axis, keepdims=axis is not None)
    elif ref == "max0":
        ref = np.max(x, axis=axis, keepdims=axis is not None)
    elif ref is None:
        ref = 1.
    else:
        raise ValueError(f"ref={ref} is invalid. ref should be either None, "
                         f"'max0' (to set maximum dB value at 0) or a function.")

    with np.errstate(divide='ignore', invalid='ignore'):
        db = 10 * np.log10(np.nan_to_num(x / ref))

    if dB_range is not None:
        db = np.clip(db + dB_range,
                     a_min=0,
                     a_max=dB_range) / dB_range

    return db


def env(S, weight=None, axis=-1, mean="ari"):
    S_max = np.max(S, axis=axis)
    if weight is not None:
        return np.sum(S * weight, axis=axis) / np.sum(weight, axis=axis)
    else:
        if mean in "arithmetic":
            S_mean = np.mean(S, axis=axis)
        elif mean in "geometric":
            S_mean = geom_mean(S, axis=axis)
        else:
            S_mean = harm_mean(S, axis=axis)
        return np.sqrt(S_mean) * S_max


def geom_mean(x, axis=None, keepdims=False):
    return np.exp(np.mean(np.where(x > 0, np.log(x), np.zeros_like(x)), axis=axis, keepdims=keepdims))


def wiener_entropy(x, axis=None):
    """
    Wiener entropy (aka spectral flatness) of an array, optionally along given axis/axes.
    Returns 1 for white noise-like patterns and 0 for single peaks.

    Note that, according to numpy rules, values of 0 passed to the log return nan, and thus removed from the mean.
    Also note that the spectral flatness returns 0 by convention if ANY bin of x is 0 (w.r.t. axis if relevant).

    Therefore, x might be clipped to a small minimum value before being passed to this function, if it makes sense for
    a given application.
    """
    return geom_mean(x, axis=axis, keepdims=axis is not None) / np.mean(x, axis=axis, keepdims=axis is not None)


def shannon_entropy(S, axis=-1, norm=True):
    """
    Shannon entropy of spectrogram S along axis.

    The axis argument should correspond to the *frequency* axis of the spectrogram S.
    """
    if norm:
        b = np.sum(S, axis=axis, keepdims=True)
        S = np.divide(S, b, out=np.zeros_like(S), where=b != 0)
    h = np.where(S != 0, S * np.log(S), np.zeros_like(S))
    h = - np.sum(h, axis=axis, keepdims=axis is not None) / np.log(S.shape[axis])
    h[np.isnan(h)] = 0
    return h


def harm_mean(x, axis=None):
    return 1 / np.mean(1 / x, axis=axis)


def norm(x, axis=None, low=None, high=None):
    if low is None:
        low = np.nanmin(x, axis=axis, keepdims=True)
    if high is None:
        high = np.nanmax(x, axis=axis, keepdims=True)
    with np.errstate(divide='ignore', invalid='ignore'):
        out = np.nan_to_num((np.clip(x, a_min=low, a_max=high) - low) / (high - low))
    return out


def moving_average(x, w, axis=-1, mode='full'):
    v = np.ones(w)
    if x.ndim == 1:
        out = np.convolve(x, v, mode=mode) / w
    else:
        out = np.apply_along_axis(lambda m: np.convolve(m, v, mode=mode), axis=axis, arr=x) / w
    return out


def rolling_func(df: pd.DataFrame,
                 mode_col: str,
                 window: str | int = 5,
                 time_cols: list | str = ['Start', 'End'],
                 group_col: None | str = None,
                 unit: str = 's',
                 # func = partial(scipy.stats.mode, nan_policy='omit')
                 ):
    """
    Computes the rolling mode (i.e. the most frequent value in a rolling, centered window).
    The rolling window can be taken either as rows (e.g. 2 will take the 2 row before and the 2 rows after the focal
        row) of the input dataframe, or as a time-based window (e.g. '10s' will take all values up to 10 seconds before
        and 10 s after the focal row).
        Note that the latter case requires also providing at least one time_cols to base the window on.

    Optionally, provided group_col in the same form as the "by" argument of the pd.Dataframe.groupby method.
    The rolling window will then be taken groupwise only.

    :param df: input pandas Dataframe
    :param window: int or str. Defines the window width.
        See the pandas.Series.rolling method docstring for further details
    :param time_cols: str or list. One or more columns of df used to determine window.
        Note that if multiple columns are provided, the row-wise mean is used.
    :param mode_col: str. Column on which to take the mode.
    :param group_col: Optional. Column that defines groups on which the mode is taken separately.

    :return: a pandas.Dataframe object with the same columns as df, but with df[mode_col] replaced by the mode.
        The order of rows and columns is unchanged, but the index is reset.
    """
    df = df.reset_index(drop=False)

    if time_cols is not None:
        if isinstance(time_cols, list):
            df['window'] = pd.to_datetime(df[time_cols].mean(axis=1), unit=unit)
        else:
            df['window'] = pd.to_datetime(df[time_cols], unit=unit)
        df['window'] = pd.Series([pd.Timestamp(_) for _ in df['window']])
        df = df.set_index('window')
        df = df.sort_index()

    if group_col is not None:
        df[mode_col] = df.groupby(by=group_col, group_keys=False)[mode_col].apply(
            lambda x: x.rolling(window=window, closed='both', center=True, min_periods=1).apply(
                lambda y: scipy.stats.mode(y, nan_policy='omit', keepdims=False)[0]
            )
        ).astype(df[mode_col].dtype)
    else:
        df[mode_col] = df[mode_col].rolling(window=window, center=True, closed='both', min_periods=1).apply(
            lambda x: scipy.stats.mode(x, nan_policy='omit', keepdims=False)[0]
        ).astype(df[mode_col].dtype)

    # remove the added index
    df = df.set_index('index')
    df = df.sort_index()
    return df


# def remove_border_objects(img, p=0, border=(True, True, True, True)):
#     """
#     cv2.floodFill fills an image with the given value (here, 0).
#     This works to remove objects that touch an array's borders.
#
#     Arguments:
#         img: 2D array. The image to process.
#         p: How far from the border to consider as still "touching the border". For instance, p=1 will also erase objects
#             that don't touch the actual border, but are 1 pixel from it.
#  ²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²       border: tuple of bool, one per side of the image: top, bottom, left right
#     """
#     h, w = img.shape
#     for row in range(h):
#         if border[0] and np.max(img[row, :(p+1)]) == 1:
#             cv2.floodFill(img, None, (np.argmax(img[row, :(p+1)]), row), newVal=0)
#         if border[1] and np.max(img[row, -(p+1):]) == 1:
#             cv2.floodFill(img, None, (np.argmax(img[row, -(p+1):]), row), newVal=0)
#     for col in range(w):
#         if border[2] and np.max(img[:(p+1), col]) == 1:
#             cv2.floodFill(img, None, (col, np.argmax(img[:max(p, 1), col])), newVal=0)
#         if border[3] and np.max(img[-(p+1):, col]) == 1:
#             cv2.floodFill(img, None, (col, np.argmax(img[-(p+1):, col])), newVal=0)
#     return img
#
#
# def mc_remove_border_objects(img, axis=0, p=0, border=(True, True, True, True)):
#     if axis != 0:
#         img = np.moveaxis(img, source=axis, destination=0)
#     ls = [remove_border_objects(_, p=p, border=border) for _ in img]
#     return np.stack(ls, axis=axis)


def check(string, seq):
    i = 0
    j = 0
    while i < len(string) and j < len(seq):
        if string[i] == seq[j]:
            j += 1
        i += 1
    return len(seq) - j


def checkall(strings, seq):
    for x in strings:
        a = check(x, seq)
        if not a == 0:
            print(x, seq, a)
            return False
    return True


def quantile(arr, q, axis=0, keepdims=False):
    """
    Written with help from ChatGPT.

    Compute the qth quantile of the input array along the specified axis.
    Parameters:
        arr (numpy.ndarray): Input array.
        q (float): The quantile to compute, between 0 and 1.
        axis (int): Axis along which to compute the quantile.

    Returns:
        numpy.ndarray: The qth quantile of the input array along the specified axis.
    """
    if q == 0:
        return np.amin(arr, axis=axis, keepdims=keepdims)
    elif q == 1:
        return np.amax(arr, axis=axis, keepdims=keepdims)

    # Compute the floor and ceiling values for the quantile
    quant = (arr.shape[axis] - 1) * q
    k = np.floor(quant).astype(int)
    kc = np.ceil(quant).astype(int)

    # Use partition to find the kth element along the specified axis
    partitioned = np.partition(arr, [k, kc], axis=axis)
    floor = np.take(partitioned, k, axis=axis)
    ceil = np.take(partitioned, kc, axis=axis)

    # Compute the weights for the linear interpolation
    weights = quant - k

    # Compute the quantile as a linear interpolation between the floor and ceiling values
    out = floor + weights * (ceil - floor)

    if keepdims:
        out = np.expand_dims(out, axis=axis)

    return out


def mlcs(strings):
    """Return a long common subsequence of the strings.
    """
    if not strings:
        raise ValueError("mlcs() argument is an empty sequence")
    strings = list(set(strings)) # deduplicate
    alphabet = set.intersection(*(set(s) for s in strings))

    # indexes[letter][i] is list of indexes of letter in strings[i].
    indexes = {letter:[[] for _ in strings] for letter in alphabet}
    for i, s in enumerate(strings):
        for j, letter in enumerate(s):
            if letter in alphabet:
                indexes[letter][i].append(j)

    # Generate candidate positions for next step in search.
    def candidates():
        for letter, letter_indexes in indexes.items():
            candidate = []
            for ind in letter_indexes:
                if len(ind) < 1:
                    break
                q = ind[0]
                candidate.append(q)
            else:
                yield candidate

    result = []
    while True:
        try:
            # Choose the closest candidate position, if any.
            pos = None
            for c in candidates():
                if not pos or sum(c) < sum(pos):
                    pos = c
            letter = strings[0][pos[0]]
        except TypeError:
            return ''.join(result)
        for let, letter_indexes in indexes.items():
            for k, ind in enumerate(letter_indexes):
                ind = [i for i in ind if i > pos[k]]
                letter_indexes[k] = ind
        result.append(letter)


def extract_triu(df, k=0, rowname='Row', colname='Column', valuename='Value'):
    """
    TODO: fix it to work with MultiIndex, let user keep columns and index names
    """
    df = df.where(np.triu(np.ones(df.shape), k=k).astype(np.bool))
    df = df.stack().reset_index()
    df.columns = [rowname, colname, valuename]
    return df