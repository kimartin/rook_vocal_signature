import os
import re
from os import cpu_count
from typing import Callable
from math import ceil
import numpy as np
import soundfile as sf
from scipy.signal import stft, istft, lfilter, sosfilt, butter, fftconvolve
import pandas as pd
from pyrle import Rle
from joblib import Parallel, delayed
import scipy.ndimage as nd

from functions.utilities import restrict_kwargs, merge_overlaps, norm, onsets_offsets, moving_average, tqdm_joblib


np.seterr(invalid='ignore', divide='ignore')


def batchsize(n, n_jobs=-1):
    """
    Given integer n, compute a batch size for the specified number of CPU cores (used for joblib.Parallel)
    """
    if n_jobs < 0:
        n_jobs = cpu_count() + n_jobs + 1
    return ceil(n / n_jobs)


def get_bit_depth(filename):
    info = sf.info(filename)
    bit_depth = info.extra_info.lower()
    if 'bit width' in bit_depth:
        bit_depth = re.sub(pattern=' +', repl=' ', string=bit_depth)
        bit_depth = re.search(pattern='(?<=bit width : )[1-9]+', string=bit_depth)
        bit_depth = int(bit_depth.group(0))
    else:
        # Ugly hack if "bit width" is not part of the extra_info attribute
        info = info.subtype_info
        if '8' in info:
            bit_depth = 8
        elif '16' in info:
            bit_depth = 16
        elif '24' in info:
            bit_depth = 24
    return bit_depth


def wav_read(file,
             start=0,
             end=None,
             units="seconds",
             sr=None,
             # tf_friendly=True,
             channel=None,
             extra=0.):
    assert units in ["seconds", "samples"], ValueError("'units' should be either 'samples' or 'seconds'")

    # Get the bit depth of the sound file to normalize the audio between -1 and 1
    # NB: this WILL throw an error if bit_depth cannot be obtained at any step of this process
    bit_depth = get_bit_depth(file)

    if sr is None:
        sr = sf.info(file).samplerate
    if units == "seconds":
        start = int(round((start - extra) * sr))
        end = int(round((end + extra) * sr)) if end is not None else None

    audio, sr = sf.read(file, start=start, stop=end, always_2d=True, dtype=np.int16)

    if channel is not None:
        audio = audio[:, channel]

    if len(audio.shape) > 1:
        audio = np.ascontiguousarray(audio.T)

    audio = audio.astype(np.float32)
    audio /= float(2 ** (bit_depth - 1))

    return audio, sr


def norm_audio(x, axis=0):
    minx = x.min(axis=axis, keepdims=True)
    maxx = x.max(axis=axis, keepdims=True)
    np.abs(minx, out=minx)
    np.abs(maxx, out=maxx)
    y = np.maximum(minx, maxx)
    x = x / y
    x[np.where(y == 0.)] = 0.
    return x


def filters(x, pre=None, dc=None, axis=-1):
    """
    Combines premphasis and dc-blocking filters with only one call to scipy.sgn.lfilter

    Both should use values between -1 and 0 (inclusive for pre, exclusive for dc).
    e.g. pre=-1, dc=-.995
    """
    if pre is None and dc is None:
        return x

    b = [1.]
    if pre is not None:
        b += pre if isinstance(pre, list) else [pre]

    a = [1.]
    if dc is not None:
        a += dc if isinstance(dc, list) else [dc]

    return lfilter(b=b, a=a, x=x, axis=axis)


def inv_filters(x, pre=None, dc=None, axis=-1):
    return filters(x, pre=dc, dc=pre, axis=axis)


def custom_filter(x, N, Wn, btype, axis=-1, sr=None):
    """
    Butterworth filter with direct application to signal x.
    Note that axis should correspond to the SAMPLE axis, not the CHANNEL axis (if relevant); e.g. for a stereo signal
    of shape (2, x), axis should be -1.

    - N: filter order (NB: basically, "how many samples prior to the current one should influence the filtering?")
    - Wn: critical frequency/ies. Can be a scalar or a tuple of length 2. Following scipy.signal conventions, should be
    floats between 0 and 1, with 1 corresponding to the Nyquist frequency of the signal (half of the sample rate).
    Can be given as a frequency is Hz, but only if sr is also given as the samplerate
    - btype: type of filter (one of: "high" for highpass, "low" for lowpass, "bandpass", or "bandstop")
    - sr: sampling rate of x, in Hz.

    We use sosfilt according to scipy.signal, for numerical stability reasons.
    """
    if sr is not None:
        if isinstance(Wn, tuple):
            Wn = tuple([_ / (sr / 2) for _ in Wn])
        else:
            Wn = Wn / (sr / 2)
    return sosfilt(butter(N, Wn, btype=btype, output='sos'), x, axis=axis, zi=None)


def smoothing_filter(n_grad_freq, n_grad_time):
    """Generates a filter to smooth the mask for the spectrogram.

    Copied verbatim from noisereduce to remove the last current call to the package, but credit where it's due.

    Arguments:
        n_grad_freq {[type]} -- [how many frequency channels to smooth over with the mask.]
        n_grad_time {[type]} -- [how many time channels to smooth over with the mask.]
    """

    filt = np.outer(
        np.concatenate(
            [
                np.linspace(0, 1, n_grad_time + 1, endpoint=False),
                np.linspace(1, 0, n_grad_time + 2),
            ]
        )[1:-1],
        np.concatenate(
            [
                np.linspace(0, 1, n_grad_freq + 1, endpoint=False),
                np.linspace(1, 0, n_grad_freq + 2),
            ]
        )[1:-1],
    )
    filt /= filt.sum()
    return filt


def get_stft(x,
             wl,
             ovlp,
             window_fn,
             sr=None,
             power=1.,
             extension_factor=1.,
             phase=False,
             ):
    """
    Get the STFT (both magnitude and phase) from a signal x.

    - wl: FFT frame length in seconds (converted to samples using sr) or in samples
    - ovlp: overlap between successive FFT frames, in % (in the form e.g .2 or 20 for 20% overlap)
    - sr: sampling rate of x, in Hz. Use ONLY for wl and ovlp computation, so can be skipped if wl is passed in samples
    - window_fn: window function to use for the STFT (e.g. "hamming")
    - pre: pre-emphasis coefficient (basically, applies: x(n) = x(n) - pre * x(n-1) to x). A good value should be
    close to but below 1 (e.g .97). Ignored if 0.
    """
    if ovlp > 1:
        ovlp /= 100

    if sr is not None:
        frame_length = int(round(wl * sr))
        frame_step = int(round(wl * sr * (1-ovlp)))
        fft_length = int(round(extension_factor * wl * sr))
    else:
        frame_length = wl
        frame_step = int(round(wl * (1-ovlp)))
        fft_length = int(extension_factor * wl)

    _, _, spec = stft(
        x,
        nperseg=frame_length,
        noverlap=frame_length - frame_step,
        nfft=fft_length,
        window=window_fn,
        padded=True,
        boundary=None,
        axis=-1
    )

    angle = None
    if phase:
        angle = np.angle(spec)

    spec = np.abs(spec)
    spec **= power

    return spec, angle


def get_istft(magnitude,
              wl,
              ovlp,
              sr,
              window_fn,
              phase=None,
              power=1.,
              extension_factor=1.,
              ):
    """
    Given both the magnitude and phase of an STFT, compute the corresponding signal by inverse STFT.

    - magnitude and phase: spectrograms of the signal to compute iSTFT for
    - all other arguments: see get_stft
    """

    if ovlp > 1:
        ovlp /= 100
    frame_length = int(round(wl * sr))
    frame_step = int(round(wl * sr * (1-ovlp)))
    fft_length = int(round(extension_factor * wl * sr))

    if power != 1.:
        magnitude = magnitude ** (1. / power)

    spec_recovered = magnitude * (np.cos(phase) + 1j * np.sin(phase))
    _, recovered = istft(
        spec_recovered,
        nperseg=frame_length,
        noverlap=frame_length - frame_step,
        nfft=fft_length,
        window=window_fn,
        boundary=False
    )

    return recovered


def get_noise_threshold(x,
                        q=None,
                        std=None,
                        axis=None,
                        stft=False,
                        **kwargs):
    """
    Computes a noise threshold from x (either a magnitude spectrogram or the raw signal.

    - q is a quantile (e.g. .95 for the 95th higher percentile), std is a number of standard deviation
        q and std are mutually exclusive, but std will override q if both a provided.
    - axis: should give the TIME axis of the STFT
    - **kwargs: allows passing get_stft arguments (see above)
    """
    if stft:
        x, _ = get_stft(x, phase=False, **kwargs)

    thresh = {}
    if q is not None:
        if not isinstance(q, list):
            q = [q]
        dc = dict(zip(q, [np.quantile(x, q=_, axis=axis, keepdims=True) for _ in q]))
        thresh.update(dc)

    if std is not None:
        if not isinstance(std, list):
            std = [std]
        mean = np.mean(x, axis=axis, keepdims=True)
        sd = np.std(x, axis=axis, keepdims=True)
        thresh.update({str(_): mean + _ * sd for _ in std})

    if len(thresh) == 1:
        thresh = list(thresh.values())[0]

    return thresh


def detect_saturation(x, bit=None):
    """
    Returns saturation (max consecutive samples and total samples) for a time series x.
    Saturation is defined as values of x between (-2**(bit-1), 2**(bit-1) - 1) if x is made of integers, or
    (-1, 1 - 1/(2**(bit - 1)) otherwise
    :param x: Array of shape ([...,] samples) representing a time series, possibly multidimensional
    :param bit: Quantisation rate of x
    :param tolerance: Tolerance for saturation. A tuple of length 2, the first corresponding to max consecutive samples
    at saturation are allowed, and the second to the max total number of samples at saturation.
    :return:
    """
    if np.max(np.abs(x)) <= 1:
        sat_min = -1
        sat_max = 1 - 1/(2**(bit - 1))
    else:
        if bit is None:
            raise ValueError("Missing bit argument")
        sat_min = -2**(bit-1)
        sat_max = 2**(bit-1) - 1

    saturation_mask = (x <= sat_min) | (x >= sat_max)

    # Get saturations
    if saturation_mask.ndim == 1:
        run = Rle(saturation_mask)
        saturations = run.runs[run.values.astype(bool)]
        saturations = [(np.max(saturations), np.sum(saturations) / saturation_mask.shape[-1]) if len(saturations) > 0 else (0, 0)]
    else:
        # assumes channels are in the first axis
        run = [Rle(_) for _ in saturation_mask]
        saturations = [r.runs[r.values.astype(bool)] for r in run]
        saturations = [(np.max(s), np.sum(s) / saturation_mask.shape[-1]) if len(s) > 0 else (0, 0) for s in saturations]

    return np.array(saturations)


def center(x, axis=None):
    return x - np.mean(x, axis=axis, keepdims=axis is not None)


def snr_(signal, noise):
    return 10 * np.log10(signal / noise)


def snr(signal, noise=None, noise_energy=None, axis=-1):
    sig_energy = center(signal, axis=axis)
    sig_energy = np.mean(sig_energy**2, axis=axis)

    if noise_energy is None:
        noise_energy = center(noise, axis=axis)
        noise_energy = np.mean(noise_energy**2, axis=axis, keepdims=noise.ndim < signal.ndim)

    return snr_(sig_energy, noise_energy)


# def framewise_snr(x, wl, ovlp, noise=None, noise_energy=None, axis=-1, sr=48000):
#     frame_length = int(sr * wl)
#     frame_step = int(round((1 - ovlp/100) * sr * wl))
#     x = tf.signal.frame(
#         x, axis=axis, frame_length=frame_length, frame_step=frame_step, pad_end=True
#     )
#     return snr(x, noise=noise, noise_energy=noise_energy, axis=axis)
#
#
# def weighted_framewise_snr(x, wl, ovlp, noise=None, noise_energy=None, axis=-1, sr=48000):
#     frame_length = int(sr * wl)
#     frame_step = int(round((1 - ovlp/100) * sr * wl))
#     x = tf.signal.frame(
#         x, axis=axis, frame_length=frame_length, frame_step=frame_step, pad_end=True
#     )
#     weights = np.mean(np.abs(x), axis=axis)
#     weights /= np.sum(weights)
#     with np.errstate(invalid='ignore', divide='ignore'):
#         return np.sum(snr(x, noise=noise, noise_energy=noise_energy, axis=axis) * weights, axis=axis) / np.sum(weights, axis)


def accumulate_noise(noise_df,
                     start,
                     end=None,
                     start_col="Start",
                     end_col="End",
                     path_col="files",
                     extend=0.,
                     accumulate_from_noise_only=False,
                     path=None,
                     transforms=None,
                     q=None,
                     std=None,
                     stft_kwargs=None,
                     return_thresholds=True,
                     return_energy=True,
                     channel_col=None,
                     ):
    """
    Given a noise_df data frame defining intervals of noise data, collects noise depending on specified arguments.

    Either end, extend, or both can be specified, with different behaviours:
        - end: if specified, noise_df will be restricted to rows overlapping the [start, end] interval
        - extend: if specified, noise_df will be restricted to rows overlapping [start-extension, start+extension]
        - both: combines the behaviours

    The path argument can be used to specify either the directory where the actual file is stored, or the path to
    the file outright. If the path_col value is not found among the columns of noise_df, the latter is assumed.
    """
    if extend == 0. and end is None:
        raise ValueError('Please provide at least one of the extend or end arguments, in the same units as start.')

    if end is None:
        end = start

    if accumulate_from_noise_only and extend > 0.:
        # Get the accumulated noise duration before and after the start point
        noise_df['dur_after'] = (noise_df[end_col] - noise_df[start_col].clip(lower=end))
        noise_df['dur_after'] = noise_df['dur_after'].clip(lower=0).cumsum()
        noise_df['dur_before'] = (noise_df[end_col].clip(upper=start) - noise_df[start_col])
        noise_df['dur_before'] = noise_df['dur_before'].clip(lower=0)[::-1].cumsum()

        # NB: we actually extend the selection by 1 row in both directions to actually REACH extension
        noise_df['dur_after'] = noise_df['dur_after'].shift(1, fill_value=0)
        noise_df['dur_before'] = noise_df['dur_before'].shift(-1, fill_value=0)

        # Select the corresponding rows of noise_df
        df = noise_df.loc[noise_df[['dur_before', 'dur_after']].max(axis=1) <= extend].reset_index(drop=True)

        # restrict the last and first rows
        df.iloc[0][start_col] = max(df.iloc[0][start_col], df.iloc[0][end_col] - extend + df.iloc[0]['dur_before'])
        df.iloc[-1][end_col] = min(df.iloc[-1][end_col], df.iloc[-1][start_col] + extend - df.iloc[-1]['dur_after'])
    else:
        df = noise_df.loc[(noise_df[end_col] >= start - extend) & (noise_df[start_col] <= end + extend)]
        df = df.reset_index(drop=True)
        df[start_col] = df[start_col].clip(lower=start - extend)
        df[end_col] = df[end_col].clip(upper=end + extend)

    if channel_col is None:
        noise_list = [wav_read(file=f'{path}/{f}' if path is not None else f,
                               start=s,
                               end=e,
                               sr=None,
                               channel=None)[0]
                      for f, s, e in df[[path_col, start_col, end_col]].itertuples(index=False)]
    else:
        noise_list = [wav_read(file=f'{path}/{f}' if path is not None else f,
                               start=s,
                               end=e,
                               sr=None,
                               channel=ch)[0]
                      for f, s, e, ch in df[[path_col, start_col, end_col, channel_col]].itertuples(index=False)]

    noise = np.concatenate(noise_list, axis=-1)

    if transforms is not None:
        noise = transforms(noise)

    # noise_spec, noise_phase = get_stft(noise, phase=True, **stft_kwargs)
    # print(noise_spec.shape)

    noise_threshold = None
    if return_thresholds:
        noise_threshold = get_noise_threshold(noise, q=q, std=std, stft=True, axis=-1, **stft_kwargs)

    noise_energy = None
    if return_energy:
        noise_energy = np.mean(center(noise, axis=0)**2, axis=-1)

    return noise, noise_threshold, noise_energy


def reduce_noise(signal,
                 noise=None,
                 sr: int = 48000,
                 wl: int | float = 1200,
                 ovlp: int | float = 75,
                 extension_factor: int | float = 1.,
                 window_fn: Callable | str = 'hamming',
                 n_grad_freq: int = 2,
                 n_grad_time: int = 4,
                 prop_decrease: float = 1.0,
                 q: None | float = None,
                 std: None | float = None,
                 noise_threshold=None,
                 return_spectrogram=False,
                 # phase: None | np.ndarray = None,
                 **kwargs,
                 ):
    """
    Largely inspired (if not outright copied and refactored) from Tim Sainburg's GitHub.
    Applies spectral gating noise reduction to a signal.

    Assumptions:
        - if working with multichannel audio, channels should be in the FIRST axis, and both signal and noise must have
        the same number of channels
        - signal and noise must have the same shape except for the last (if passed as sound data) or the last two (if
        passed as spectrograms) axes
        - if using noise_threshold directly, the same assumptions apply, and the last axis of noise_threshold must be
        the same as the last axis of the STFT shape of signal (corresponding to number of frequency bins)

        - signal and noise can be passed as any combination of sound data and spectrogram provided the above
        is verified

        For instance:
            - signal (4, SAMPLES) or (4, TIME_BINS, FREQ_BINS) and noise (4, SAMPLES): OK so long as the STFT arguments
            result in noise of shape (4, TIME_BINS_2, FREQ_BINS), and vice-versa
            - signal (4, SAMPLES) and noise (3, SAMPLES): FAIL
            - signal (SAMPLES, ) and noise (SAMPLES,): OK
            - signal (TIME_BINS, FREQ_BINS) and noise (4, SAMPLES): FAIL
            - signal (TIME_BINS, FREQ_BINS) and noise (SAMPLES, ): OK so long as STFT arguments result in noise
            (TIME_BINS_2, FREQ_BINS)

    Important note: this function was meant for use with signal and noise coming from the same overall file. In
    particular, they should have the same sampling rate, otherwise results will be nonsensical (but we have no way
    to detect it from within the function).

    :param signal: array-like. Signal to clean.
    :param noise: array-like. Noise used for the noise reduction. Ignored if noise_threshold is not None.
    :param noise_threshold: array-like. Can be passed instead of noise, containing the noise threshold spectrum.
    Must be broadcastable with the signal spectrogram, otherwise an error is thrown.
    :param sr: sampling rate for both signal and noise.
    :param wl: float > 0. Length (in seconds) of each STFT frame.
    :param ovlp: float or int between 0 and 100. Overlap between successive frames. If > 1, interpreted as a percentage
    and divided by 1000, otherwise used as is.
    :param extension_factor: int or float. Zero padding factor for the STFT. In practice, determines the nfft argument
    of scipy.signal.stft through the formula nfft = int(wl * sr * extension_factor).
    :param window_fn: callable or str. Window function for the STFT.
    :param n_grad_freq: int > 0. Number of frequence bins for the smoothing filter
    :param n_grad_time: int > 0. Number of time bins for the smoothing filter.
    :param prop_decrease: float between 0 and 1. Proportion of the noise to erase from the spectrogram.
    :param pre: float. Preemphasis coefficient (see preemphasis and inv_preemphasis). Ignored if 0. If > 0., preemphasis
    will be applied to both signal and noise before the noise reduction, and the recovered signal will go through the
    reverse process.
    :param q: float between 0 and 1. Quantile of the noise distribution used to set the spectral gating threshold.
    Mutually exclusive with std (an error WILL be thrown if both are not None).
    :param std: float above 0. Number of standard deviation above the mean used to set the spectral gating threshold.
    Mutually exclusive with q (an error WILL be thrown if both are not None).
    :param return_spectrogram: bool. If False, get the ISTFT to return the signal in waveform. If True, returns the
    spectrogram after denoising.
    :param kwargs: Argument buffer (allows passing extra arguments without crashing)

    :return: array-like of the same shape as signal, corresponding to the noise-reduced signal. Only if recover_noise is
    True, a tuple of 2 array-like, one of the same shape as signal, the other of the same shape as noise
    """
    if q is not None and std is not None:
        raise ValueError("Provide only one of: q (upper quantile) or std (number of standard deviations above mean).")

    samples = signal.shape[-1]

    # Automatically get arguments for each sub-function
    stft_args = restrict_kwargs(get_stft, locals())
    istft_args = restrict_kwargs(get_istft, locals())
    noise_threshold_args = restrict_kwargs(get_noise_threshold, locals())

    # Spectrogram, unless both signal and phase are passed as arrays of the same shape
    spec, phase = get_stft(signal, phase=True, **stft_args)

    # Threshold
    if noise_threshold is None:
        if noise is None:
            noise = signal

        if len(spec.shape) != len(noise.shape):
            # if both are spectrograms and compatible, then here both shapes should have same length
            # if False, then noise was passed as a sound, and we get its spectrogram
            noise, _ = get_stft(noise, phase=False, **stft_args)

        noise_threshold = get_noise_threshold(noise, **noise_threshold_args, axis=-1)

    # Apply threshold to the spectrogram
    sig_mask = (spec > noise_threshold).astype('float32')

    # Remove objects that touch the borders of the spectrogram, only on the time axis (i.e. objects before and after
    # the vocalisation)
    # sig_mask = mc_remove_border_objects(sig_mask, axis=0, p=2, border=(False, False, True, True))

    # First smoothing operation: closing removes small holes, opening removes small objects
    if sig_mask.ndim == 2:
        structure = np.array([[0, 1, 0],
                              [1, 1, 1],
                              [0, 1, 0]])
    else:
        structure = np.zeros(shape=(3, 3, 3))
        structure[1] = [[0, 1, 0],
                        [1, 1, 1],
                        [0, 1, 0]]
    sig_mask = nd.binary_closing(sig_mask, structure=structure, iterations=1)
    sig_mask = nd.binary_opening(sig_mask, structure=structure, iterations=1)

    # Gaussian smoothing
    if n_grad_time + n_grad_freq > 0:
        smoother = smoothing_filter(n_grad_freq, n_grad_time)
        smoother = smoother.T
        if len(spec.shape) > 2:
            smoother = np.expand_dims(smoother, axis=0)

        # smooth the mask
        sig_mask = fftconvolve(sig_mask, smoother, mode="same") * prop_decrease

    # mask the signal
    sig_stft_amp = spec * sig_mask

    if return_spectrogram:
        return sig_stft_amp

    # invert the STFT
    recovered = get_istft(
        magnitude=sig_stft_amp,
        phase=phase,
        **istft_args
    )

    # restrict to original length
    recovered = recovered[..., :samples]

    return recovered


def noise_reduction(df,
                    wl,
                    ovlp,
                    window_fn,
                    sr=48000,
                    pre=0.,
                    q=None,
                    std=None,
                    prop_decrease=1.,
                    extension_factor=1.,

                    n_grad_freq=4,
                    n_grad_time=4,

                    minimum_noise=0.,
                    maximum_interval=0.,
                    maximum_noise_extend=10.,
                    transforms=None,

                    db_range=None,
                    power=1.,
                    use_tensorflow=False,

                    start_col='Start',
                    end_col='End',
                    path_col='file',
                    wav_col='wav',
                    group_col="bout",
                    denoised_col='denoised',
                    channel_col = None,

                    return_noises=False,
                    return_snr=False,

                    n_jobs=-1,

                    ):
    """
    Essentially wrapper function for reduce_noise, applied to an entire dataframe.
    Noises are computed from the intervals between the rows of the dataframe.
    """
    # path = df.loc[0, path_col]
    df = df.reset_index(drop=True)

    # Automatically fetch arguments for each sub-function from the arguments passed to noise_reduction
    stft_args = restrict_kwargs(get_stft, locals())
    reduce_noise_args = restrict_kwargs(reduce_noise, locals())
    noise_threshold_args = restrict_kwargs(get_noise_threshold, locals())
    parallel_args = dict(n_jobs=n_jobs,
                         batch_size=batchsize(df.shape[0], n_jobs=n_jobs),
                         pre_dispatch=2*os.cpu_count() if n_jobs == -1 else '2*n_jobs',
                         verbose=0)

    # Ignore rows with no data for denoising, but not for accumulate_noise
    idx = df.loc[~df[wav_col].isnull()]
    idx_len = idx.shape[0]
    idx = idx.index

    # Noises are taken as the complement of all vocalizations (see merge_overlaps documentation)
    noise = merge_overlaps(df, start=start_col, end=end_col, by=[path_col])
    noise = pd.concat(
        [
            pd.DataFrame({
                path_col: group.loc[0, path_col],
                start_col: [0] + group[end_col].tolist(),
                end_col: group[start_col].tolist() + [sf.info(group.loc[0, path_col]).duration]
            }) for _, group in noise.groupby(path_col)
        ],
        ignore_index=True
    )

    # Discard short intervals of noise
    if minimum_noise > 0.:
        noise = noise.loc[noise[end_col] - noise[start_col] > minimum_noise]
        noise = noise.reset_index(drop=True)

    # Get start and end of each noise sample
    if group_col is None:
        # Each unit is denoised independently
        group_col = "group"
        noise_collection = df[[start_col, end_col]]
    else:
        df[group_col] = df[group_col].factorize()[0]
        noise_collection = df.groupby(by=[group_col]).agg({start_col: 'min', end_col: 'max'})

    # Apply transformations such as pre-emphasis, normalisation, or filtering
    if transforms is not None:
        with tqdm_joblib(desc="Transforms",
                         total=idx_len,
                         position=0,
                         leave=True) as progress_bar:
            df[wav_col] = pd.Series(
                Parallel(**parallel_args)(delayed(transforms)(_) for _ in df.loc[idx, wav_col]),
                index=idx
            )

    # Reads in the noise data
    with tqdm_joblib(desc="Accumulating noise",
                     total=noise_collection.shape[0],
                     position=0,
                     leave=True) as progress_bar:
        noise_stuff = Parallel(**parallel_args)(delayed(accumulate_noise)(noise_df=noise,
                                                                          start=start,
                                                                          end=end,
                                                                          extend=maximum_noise_extend,
                                                                          start_col=start_col,
                                                                          end_col=end_col,
                                                                          path_col=path_col,
                                                                          # channel_col could save some processing but
                                                                          # if have group_col it can be dangerous
                                                                          # as the channel selection may discard some
                                                                          # noises we want to actually use
                                                                          channel_col=None,
                                                                          path=None,
                                                                          transforms=transforms,
                                                                          stft_kwargs=stft_args,
                                                                          **noise_threshold_args)
                                                for start, end in noise_collection.itertuples(index=False))
    noises, noises_threshold, noises_energy = zip(*noise_stuff)

    # Noise reduction algorithm
    with tqdm_joblib(desc="Reducing noise",
                     total=idx_len,
                     position=0,
                     leave=True) as progress_bar:
        denoised = Parallel(**parallel_args)(
            delayed(reduce_noise)(signal=sig,
                                  noise_threshold=noises_threshold[group][df.loc[idx, channel_col].iloc[i]] if channel_col else noises_threshold[group],
                                  **reduce_noise_args)
            for i, sig, group in df.loc[idx, [wav_col, group_col]].itertuples(index=True)
        )

    with tqdm_joblib(desc="Wav energy",
                     total=idx_len,
                     position=0,
                     leave=True) as progress_bar:
        wav_col_energy = Parallel(**parallel_args)(
            delayed(lambda s: np.mean(center(s, axis=-1) ** 2, axis=-1))(s)
            for s in df.loc[idx, wav_col]
        )

    with tqdm_joblib(desc="Denoised energy",
                     total=idx_len,
                     position=0,
                     leave=True) as progress_bar:
        denoised_col_energy = Parallel(**parallel_args)(
            delayed(lambda s: np.mean(center(s, axis=-1) ** 2, axis=-1))(s)
            for s in denoised
        )

    out_data = pd.DataFrame(
        {
            denoised_col: denoised,
            f'{wav_col}_energy': wav_col_energy,
            f'{denoised_col}_energy': denoised_col_energy,
            f'noise_energy': [noises_energy[i] for i in df.loc[idx, group_col]]
        },
        index=idx
    )
    for col in out_data.columns:
        df[col] = out_data[col]

    if return_noises:
        with tqdm_joblib(desc="Denoising noise",
                         total=len(noises),
                         position=0,
                         leave=True) as progress_bar:
            denoised_noises = Parallel(**parallel_args)(
                delayed(reduce_noise)(signal=noise_, noise_threshold=noises_threshold_, **reduce_noise_args)
                for noise_, noises_threshold_ in zip(noises, noises_threshold)
            )
        with tqdm_joblib(desc="Denoised noise energy",
                         total=len(noises),
                         position=0,
                         leave=True) as progress_bar:
            denoised_noise_energy = Parallel(**parallel_args)(
                delayed(lambda n: np.mean(center(n, axis=-1) ** 2, axis=-1))(n)
                for n in denoised_noises
            )
        noise_data = pd.DataFrame({f'{denoised_col}_noise_energy': [denoised_noise_energy[i] for i in df.loc[idx, group_col]]}, index=idx)
        for col in out_data.columns:
            df[col] = out_data[col]

    if return_snr:
        df = get_snr_columns(df, wav_col=wav_col, denoised_col=denoised_col, noise_col='noise')

    return df


def get_snr_columns(df,
                    wav_col='wav',
                    denoised_col='denoised',
                    noise_col='noise',
                    ):
    parallel_args = dict(
        n_jobs=-1,
        batch_size=batchsize(df.shape[0], n_jobs=-1),
        pre_dispatch=2*os.cpu_count(),
        verbose=0,
    )

    # Ignore rows with no data
    idx = df.loc[~df[wav_col].isnull()].index

    denoised_noise_energy_col = f'{denoised_col}_{noise_col}_energy'
    noise_energy_col = f'{noise_col}_energy'
    wav_energy_col = f'{wav_col}_energy'
    denoised_energy_col = f'{denoised_col}_energy'

    denoised_noise_energy_col = denoised_noise_energy_col if denoised_noise_energy_col in df.columns else noise_energy_col

    columns = [wav_energy_col, denoised_energy_col, noise_energy_col, denoised_noise_energy_col]
    column_in_df = [_ not in df.columns for _ in columns]
    if any(column_in_df):
        raise ValueError(f'Missing columns in df: {[c for c, in_df in zip(columns, column_in_df) if in_df]}')

    with Parallel(**parallel_args) as par:
        wav_snr = par(
            delayed(snr_)(signal=sig_e, noise=noise_e)
            for sig_e, noise_e in df.loc[idx, [wav_energy_col, noise_energy_col]].itertuples(index=False)
        )

    with Parallel(**parallel_args) as par:
        denoised_snr = par(
            delayed(snr_)(signal=sig_e, noise=noise_e)
            for sig_e, noise_e in
            df.loc[idx, [denoised_energy_col, denoised_noise_energy_col]].itertuples(index=False)
        )

    with Parallel(**parallel_args) as par:
        improvement_snr = par(
            delayed(snr_)(signal=denoised, noise=(original - denoised))
            for original, denoised in df.loc[idx, [wav_energy_col, denoised_energy_col]].itertuples(index=False)
        )
        # print(improvement_snr)
    snr_data = pd.DataFrame(
        {
            f'{wav_col}_snr': wav_snr,
            f'{denoised_col}_snr': denoised_snr,
            'improvement_snr': improvement_snr
        },
        index=idx
    )
    for col in snr_data.columns:
        df[col] = snr_data[col]
    return df


def refine(onsets,
           offsets,
           env,
           trim=-1,
           min_to_keep=0.,
           merge_vocs_separated_by = 0.,
           shortest_voc=0,
           method=np.max):

    mask = offsets - onsets > 0
    onsets = onsets[mask]
    offsets = offsets[mask]

    # Remove detections that don't go high enough
    if len(onsets) > 0 and min_to_keep > 0.:
        mask = [method(env[on:off]) > min_to_keep for on, off in zip(onsets, offsets)]
        onsets = onsets[mask]
        offsets = offsets[mask]

    # Merge vocs separated by less than shortest_silence
    if len(onsets) > 1 and merge_vocs_separated_by > 0.:
        short_silences = onsets[1:] - offsets[:-1] > merge_vocs_separated_by
        onsets = onsets[np.insert(short_silences, 0, True)]
        offsets = offsets[np.append(short_silences, True)]

    # Remove remaining vocs shorter than shortest_voc
    if len(onsets) > 1 and shortest_voc > 0.:
        short_vocs = offsets - onsets > shortest_voc
        onsets = onsets[short_vocs]
        offsets = offsets[short_vocs]

    # Trim
    if len(onsets) > 1 and trim >= 0:
        mask = (offsets > trim) * (onsets < env.shape[-1] - 1 - trim)
        onsets = onsets[mask]
        offsets = offsets[mask]

    return np.array([onsets, offsets])


def segment(env,
            threshold=.05,
            merge_vocs_separated_by=0,
            shortest_voc=0,
            min_to_keep=0.,
            method=np.max,  # partial(np.quantile, q=.95)
            trim=-1,
            smooth=0,
            ):
    """
    Given an envelope env, segment into one or more vocalizations.

    - env: envelope. Should be a 1D array. Will be normalized to the [0, 1] range.
    - threshold: cutoff values for env segmentation (values below this threshold will be considered non-vocalizations)
    - merge_vocs_separated_by and shortest_voc: remove/merge vocalizations. Both are in frames of env.
        merge_vocs_separated_by is applied first and removes silences shorter than it
        then shortest_voc removes vocalizations shorter than it
    - min_to_keep: Vocalizations where the envelope is above threshold but with at least one value above this are
        considered too weak and removed. Shouldn't be too high above threshold to avoid spuriously removing
        vocalizations.
    - method: function to determines whether env values are above min_to_keep
    """
    # Get runs of enveloppe > threshold
    env = norm(env, axis=-1)
    if smooth > 0:
        env = moving_average(env, w=smooth, axis=-1)

    signal = env > threshold

    # Get onsets and offsets
    if signal.ndim == 1:
        onsets, offsets = onsets_offsets(signal)
        out = refine(onsets, offsets, env,
                     trim=trim,
                     min_to_keep=min_to_keep,
                     merge_vocs_separated_by=merge_vocs_separated_by,
                     shortest_voc=shortest_voc,
                     method=method)
    else:
        onsets, offsets = zip(*[onsets_offsets(s) for s in signal])
        out = [refine(on, off, en,
                      trim=trim,
                      min_to_keep=min_to_keep,
                      merge_vocs_separated_by=merge_vocs_separated_by,
                      shortest_voc=shortest_voc,
                      method=method) for on, off, en in zip(onsets, offsets, env)]

    return out

