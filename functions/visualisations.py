import io
import seaborn as sns
import numpy as np
import pandas as pd
import os
import soundfile as sf
from joblib import Parallel, delayed
import math

from typing import List, Dict, Optional
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.lines import Line2D
from matplotlib.collections import LineCollection, PolyCollection

from scipy.stats import zscore
import scipy.cluster.hierarchy as sch

from bokeh.plotting import figure, show, output_file
from bokeh.models import ColumnDataSource, CustomJS, HoverTool, Tabs, CategoricalColorMapper, ContinuousColorMapper
from bokeh.palettes import Viridis256, Category20
from bokeh.transform import linear_cmap, factor_cmap
from bokeh.layouts import gridplot
from PIL import Image
from io import BytesIO
import base64
from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import cm, pyplot as plt, gridspec, colors as col
from skimage import img_as_ubyte
from scipy.spatial import cKDTree, Voronoi, voronoi_plot_2d


def plot_and_play_channels(
        label,
        n=20,
        sr=48000,
        hideaxes=True,  # If True, the axes will be suppressed
        audio_col="wav",
        spec_col="spectrogram",
        env_col="envelope",
        channel_col="channels",
        segment_col="onsets_offsets",
        annotation_cols=None,
        random_state=None,
        ncols=None,
        sizing_mode="scale_width",
):
    """
    This function plots the spectrograms in the label dataframe interactively using Bokeh.
    The vocal envelope, channel chosen and the segmentation can also be displayed by providing the relevant columns.
    Finally, the corresponding audio can be played by proving the relevent column.

    Further information can be included in a HoverTool.

    Multi-channel audio is handled (and, in fact, expected) by stacking the data by channel such that the first channel
    will be at the bottom of the plot.

    Data format is expected as channels first, i.e. all data should be of the shape [channel_axis, *relevant_axes]

    :param label: DataFrame object from which the plots are done. Column names should be actively provided in the below
    arguments.
    :param n: int. If provided as a number between 0 and label.shape[0], randomly sample n rows from label to plot.
    n = -1 or n > label.shape[0] have no effect, and the entire label will be plotted, which can be expensive in memory.
    :param sr: int. The sample rate of the audio. Useful only if providing audio_col below.
    :param hideaxes: bool. If True, hide the plot axes, otherwise leave them as default.
    :param spec_col: Column name corresponding to the spectrogram data.

    The below arguments are optional, and serve to add further information to the plots.

    :param audio_col: Column name corresponding to the audio data. Will be played on click on the relevant plot, down to
    the correct channel if plotting multichannel audio.
    :param env_col: Column name corresponding to the vocal envelope data.
    :param channel_col: Column name corresponding to the channel data (i.e. which channel should be kept)
    :param segment_col: Column name corresponding to segmentation data
    :param annotation_cols: Optional list of column names that will be passed as tooltips to HoverTool and displayed
    accordingly.
    """
    multichannel = False

    channel_axis = 0
    freq_axis = -2
    time_axis = -1

    if annotation_cols is None:
        annotation_cols = []

    label = label.loc[[s is not None for s in label[spec_col]]].reset_index(drop=True)
    if 0 < n < label.shape[0]:
        label = label.iloc[:n].reset_index()
        # label = label.sample(n=n, random_state=random_state).reset_index()

    dim_spec = [spec.shape for spec in label[spec_col]]

    if label[spec_col].iloc[0].ndim == 3:
        # Stack channels along the frequency axis
        label[spec_col] = [np.concatenate(list(spec), axis=freq_axis - 1) for spec in label[spec_col]]
        # set multichannel flag
        multichannel = True

    children = []
    for i in range(label.shape[0]):
        spec_data = ColumnDataSource(data=dict(image=[label[spec_col].iloc[i]]))

        if len(annotation_cols) > 0:
            def format_annotations(ann):
                if isinstance(ann, np.ndarray):
                    return str(np.round(np.nan_to_num(ann, copy=False, nan=-np.inf), 2))
                elif pd.isna(ann):
                    return None
                elif not isinstance(ann, str):
                    return str(ann)
                else:
                    return ann

            annotation_data = dict(zip(annotation_cols,
                                       [[format_annotations(_)] for _ in label[annotation_cols].iloc[i]]))
            spec_data.data.update(annotation_data)

        # Initialize figure
        child = figure(
            x_range=(0, spec_data.data['image'][0].shape[1]),
            y_range=(0, spec_data.data['image'][0].shape[0]),
            width=200, height=200,
            tools=["xwheel_zoom, pan, reset"]
        )

        # child.sizing_mode = sizing_mode
        # Hide axes for all plots
        if hideaxes:
            child.axis.major_tick_line_color = None
            child.axis.minor_tick_line_color = None
            child.axis.major_label_text_font_size = '0pt'
            child.axis.visible = False

        # Display spectrograms
        p = child.image(image='image',
                        x=0,
                        y=0,
                        dw=spec_data.data['image'][0].shape[1],
                        dh=spec_data.data['image'][0].shape[0],
                        palette="Viridis256",
                        source=spec_data)

        # Prepare annotations on hover:
        if len(annotation_cols) > 0:
            hover = HoverTool()
            hover.tooltips = [(col, f'@{col}') for col in annotation_cols]
            child.tools.append(hover)
            hover.renderers = [p]

        # Separate channels with white lines
        if multichannel:
            child.multi_line(xs=[[0, child.x_range.end] for _ in range(dim_spec[i][channel_axis])],
                             ys=[[dim_spec[i][freq_axis] * _] * 2 for _ in range(dim_spec[i][channel_axis])],
                             color="white",
                             line_width=1)
        children.append(child)

    # Add envelope
    if env_col is not None and env_col in label.columns:
        if multichannel:
            for i, child in enumerate(children):
                env_data = label[env_col].iloc[i]
                env_data = ColumnDataSource(
                    data=dict(
                        xs=[list(0.5 + np.arange(dim_spec[i][time_axis])) for _ in range(dim_spec[i][channel_axis])],
                        # Adjust enveloppe values, so they're
                        # 1) on the same scale as the spectrograms, and
                        # 2) offset correctly by channel
                        ys=[(env_data[_] + _) * dim_spec[i][freq_axis] for _ in range(dim_spec[i][channel_axis])]))
                child.multi_line(source=env_data,
                                 xs="xs",
                                 ys="ys",
                                 color="orange",
                                 line_width=1)
        else:
            for i, child in enumerate(children):
                env_data = label[env_col].iloc[i]
                env_data = ColumnDataSource(
                    data=dict(x=0.5 + np.arange(dim_spec[i][time_axis]),
                              y=env_data * dim_spec[i][freq_axis]))
                child.line(source=env_data,
                           x="x",
                           y="y",
                           color="orange",
                           line_width=1)

    # Apply callback to play audio
    if audio_col is not None and audio_col in label.columns:
        for i, child in enumerate(children):
            aud = label[audio_col].iloc[i]
            aud = np.array(aud)
            if aud.ndim == 1:
                aud = np.expand_dims(aud, axis=0)
            audio_data = ColumnDataSource(data=dict(audio=list(aud)))

            child.js_on_event('tap',
                              CustomJS(args=dict(source=audio_data),
                                       code="""
                                           var sr = %d;
                                           var size = %d;

                                           var channel = Math.floor(cb_obj.y / size);
                                           console.log(source.data['audio']);

                                           var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
                                           var myArrayBuffer = audioCtx.createBuffer(1, source.data['audio'][channel].length, sr);
                                           var currentChannel = source.data['audio'][channel];

                                           for (var channel = 0; channel < myArrayBuffer.numberOfChannels; channel++) {
                                                var nowBuffering = myArrayBuffer.getChannelData(channel);
                                                for (var i = 0; i < myArrayBuffer.length; i++) {
                                                    nowBuffering[i] = currentChannel[i];
                                                }
                                            }

                                            var source = audioCtx.createBufferSource();
                                            // set the buffer in the AudioBufferSourceNode
                                            source.buffer = myArrayBuffer;
                                            // connect the AudioBufferSourceNode to the
                                            // destination so we can hear the sound
                                            source.connect(audioCtx.destination);
                                            // start the source playing
                                            source.start();
                                            """ % (sr, dim_spec[i][freq_axis])))

    if channel_col is not None and channel_col in label.columns:
        for i, child in enumerate(children):
            channel_data = label[channel_col].iloc[i]
            if np.isfinite(channel_data):
                fq = dim_spec[i][freq_axis]
                tm = dim_spec[i][time_axis]
                child.segment(y0=[channel_data * fq, (channel_data + 1) * fq, channel_data * fq, channel_data * fq],
                              y1=[channel_data * fq, (channel_data + 1) * fq, (channel_data + 1) * fq,
                                  (channel_data + 1) * fq],
                              x0=[0, 0, 0, tm],
                              x1=[tm, tm, 0, tm],
                              color="red",
                              line_width=2)

    if segment_col is not None and segment_col in label.columns:
        for i, child in enumerate(children):
            segment_data = label[segment_col].iloc[i]
            if segment_data is not None:
                if channel_col in label.columns and not np.isnan(label.loc[i, channel_col]):
                    segment_source = segment_data[label.loc[i, channel_col]]
                    segment_source = ColumnDataSource(data=dict(
                        x=np.mean(segment_source, axis=0),
                        y=dim_spec[i][freq_axis] * (label.loc[i, channel_col] + .5) * np.ones(segment_source.shape[1]),
                        width=segment_source[1] - segment_source[0],
                        height=dim_spec[i][freq_axis] * np.ones(segment_source.shape[1])
                    ))
                    child.rect(x="x",
                               y="y",
                               width="width",
                               height="height",
                               source=segment_source,
                               fill_alpha=.4,
                               color="red")
                else:
                    segment_source = ColumnDataSource(data=dict(
                        x=np.mean(segment_data, axis=0),
                        y=dim_spec[i][freq_axis] * .5 * np.ones(segment_data.shape[1]),
                        width=segment_data[1] - segment_data[0],
                        height=dim_spec[i][freq_axis] * np.ones(segment_data.shape[1])
                    ))
                    child.rect(x="x",
                               y="y",
                               width="width",
                               height="height",
                               fill_alpha=.4,
                               color="red",
                               source=segment_source,
                               )

    if ncols is None:
        ncols = math.ceil(len(children) / 4)
    grid = gridplot(children=children, ncols=ncols)
    grid.sizing_mode = sizing_mode
    show(grid)


def to_png(arr):
    out = BytesIO()
    im = Image.fromarray(arr)
    im.save(out, format='png')
    return out.getvalue()


def b64_image_files(images, cmap='magma'):
    cmap = cm.get_cmap(cmap)
    urls = []
    for im in images:
        png = to_png(img_as_ubyte(cmap(im)))
        url = 'data:image/png;base64,' + base64.b64encode(png).decode('utf-8')
        urls.append(url)
    return urls


def _save_image(data, filename, cmap='inferno'):
    """https://fengl.org/2014/07/09/matplotlib-savefig-without-borderframe/"""
    sizes = np.shape(data)
    height = float(sizes[0])
    width = float(sizes[1])
    fig = plt.figure()
    fig.set_size_inches(width / height, 1, forward=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(data, cmap=cmap, origin='lower')
    plt.savefig(filename, dpi=height)
    plt.close('all')


def _write_images(images, output_dir='temp', num_imgs=-1, n_jobs=-1):
    """
    Writes images as JPG files to the specified directory
    """
    if num_imgs == -1:
        idx = np.arange(len(images))
    else:
        idx = np.random.choice(range(len(images)), size=num_imgs)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with Parallel(n_jobs=n_jobs, pre_dispatch='all') as p:
        p(delayed(_save_image)(images[i], f'{output_dir}/{str(i)}.jpg') for i in idx)


def _write_sounds(sounds, output_dir='temp', num_sounds=-1, sr=48000, n_jobs=-1):
    if num_sounds == -1:
        idx = np.arange(len(sounds))
    else:
        idx = np.random.choice(range(len(sounds)), size=num_sounds)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with Parallel(n_jobs=n_jobs, pre_dispatch='all') as p:
        p(delayed(sf.write)(f'{output_dir}/{str(i)}.wav', sounds[i], sr) for i in idx)


def scatter_projections(
        projection,
        labels=None,
        ax=None,
        figsize=(10, 10),
        alpha=0.1,
        s=1,
        color="k",
        color_palette="hls",
        categorical_labels=True,
        show_legend=True,
        tick_pos="bottom",
        tick_size=16,
        cbar_orientation="vertical",
        log_x=False,
        log_y=False,
        grey_unlabelled=True,
        fig=None,
        colornorm=False,
        rasterized=True,
        equalize_axes=False,
        facecolor='black',
        sort_labels=True,
        print_lab_dict=False,  # prints color scheme
        legend_kwargs={'bbox_to_anchor': (1.1, .5), 'loc': "right"},
):
    """
    creates a scatterplot of syllables using some projection
    """
    # color labels
    if labels is not None:
        labels = pd.Series(labels)
        if categorical_labels:
            pal = sns.color_palette(color_palette, n_colors=labels.nunique())
            lab = labels.unique()
            if sort_labels:
                lab = sorted(lab)
            lab_dict = {lab: pal[i] for i, lab in enumerate(lab)}
            if grey_unlabelled:
                if -1 in lab_dict.keys():
                    lab_dict[-1] = [0.95, 0.95, 0.95, 1.0]
                if print_lab_dict:
                    print(lab_dict)
            colors = np.array([lab_dict[i] for i in labels], dtype=object)
    elif facecolor is not None:
        c = list(col.to_rgba(facecolor))
        c[:-1] = [1. - _ for _ in c[:-1]]
        colors = tuple(c)
    else:
        colors = color

    if ax is None:
        if projection.shape[1] == 2:
            fig, ax = plt.subplots(figsize=figsize)
        elif projection.shape[1] == 3:
            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(projection='3d')

    # plot
    if colornorm:
        norm = col.LogNorm()
    else:
        norm = None

    if categorical_labels or labels is None:
        dc = dict(
            c=colors,
            norm=norm
        )
    else:
        dc = dict(
            vmin=np.quantile(labels, 0.01),
            vmax=np.quantile(labels, 0.99),
            cmap=plt.get_cmap(color_palette),
        )

    sct = ax.scatter(
        x=projection[:, 0],
        y=projection[:, 1],
        rasterized=rasterized,
        alpha=alpha,
        s=s,
        marker='.',
        **dc
    )

    # Set background color (more useful if given labels)
    ax.set_facecolor(facecolor)

    if log_x:
        ax.set_xscale("log")
    if log_y:
        ax.set_yscale("log")

    if labels is not None and show_legend:
        if not categorical_labels:
            if cbar_orientation == "horizontal":
                axins1 = inset_axes(
                    ax,
                    width="50%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc="upper left",
                )
            else:
                axins1 = inset_axes(
                    ax,
                    width="5%",  # width = 5% of parent_bbox width
                    height="50%",  # height : 50%
                    loc="lower right",
                )
            cbar = plt.colorbar(sct, cax=axins1, orientation=cbar_orientation)
            cbar.ax.tick_params(labelsize=tick_size)
            axins1.xaxis.set_ticks_position(tick_pos)
        else:
            legend_elements = [
                Line2D([0], [0], marker=".", color=value, label=key)
                for key, value in lab_dict.items()
            ]
            ax.legend(handles=legend_elements, **legend_kwargs)
    if equalize_axes:
        ax.axis("equal")

    # Turns off axes and ticks but keeps facecolor
    for spine in ax.spines.values():
        spine.set_visible(False)
    ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)

    return ax


# The following 3 functions detect if two segments (defined by the coordinates of their extremities) intersect
# Used in modified scatter_spec to avoid lines intersecting each other
def onSegment(p, q, r):
    # checks if three points p, q, and r are on the same line
    if ((q[0] <= max(p[0], r[0])) and (q[0] >= min(p[0], r[0])) and
            (q[1] <= max(p[1], r[1])) and (q[1] >= min(p[1], r[1]))):
        return True
    return False


def orientation(p, q, r):
    # to find the orientation of an ordered triplet (p,q,r)
    # function returns the following values:
    # 0 : Colinear
    # 1 : Clockwise
    # 2 : Counterclockwise

    # See https://www.geeksforgeeks.org/orientation-3-ordered-points/amp/
    # for details of below formula.
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val > 0:
        # Clockwise orientation
        return 1
    elif val < 0:
        # Counterclockwise orientation
        return 2
    else:
        # Colinear orientation
        return 0


# The main function that returns true if the line segment 'p1q1' and 'p2q2' intersect.
def doIntersect(p1, q1, p2, q2):
    # Find the 4 orientations required for the general and special cases
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # General case
    if o1 != o2 and o3 != o4:
        # lines intersect
        return True

    # Special Cases
    # p1 , q1 and p2 are colinear and p2 lies on segment p1q1
    if o1 == 0 and onSegment(p1, p2, q1):
        return True
    # p1 , q1 and q2 are colinear and q2 lies on segment p1q1
    if o2 == 0 and onSegment(p1, q2, q1):
        return True
    # p2 , q2 and p1 are colinear and p1 lies on segment p2q2
    if o3 == 0 and onSegment(p2, p1, q2):
        return True
    # p2 , q2 and q1 are colinear and q1 lies on segment p2q2
    if o4 == 0 and onSegment(p2, q1, q2):
        return True

    # If none of the cases: lines do not intersect
    return False


def static_scatter_spec(
        z,
        specs,
        labels=None,
        column_size=10,
        pal_color="hls",
        matshow_kwargs={"cmap": 'inferno'},
        scatter_kwargs={"alpha": 0.5, "s": 1},
        line_kwargs={"lw": .1, "ls": "dashed", "alpha": 1, 'color': 'white'},
        color_points=False,
        figsize=(10, 10),
        range_pad=0.1,
        x_range=None,
        y_range=None,
        enlarge_points=0,
        draw_lines=True,
        n_subset=-1,
        ax=None,
        show_scatter=True,
        show_legend=True,
        border_line_width=1,
        border_line_color="white",
        img_origin="lower",
        hide_axes=True,
):
    """
    Plots both embedding results and a selection of spectrograms corresponding to particular points, optionally linking
    each pair with a line.

    z: embedding of shape (n_samples, p >= 2). Only the first two columns of z will be plotted, however
    specs: list of spectrograms to display. Each element of the list should correspond to a row of z.
    column_size: controls the total number of spectrograms plotted in the output (surrounds the projection with a grid
        of spectrograms of size column_size
    """
    # number of spectrograms
    n_columns = column_size * 4 - 4

    # color palette
    if labels is None:
        pal = sns.color_palette(pal_color, n_colors=n_columns)
    else:
        pal = sns.color_palette(pal_color, n_colors=pd.Series(labels).nunique())

    # instantiate figure object and the subplots
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(column_size, column_size)

    # Determines the ranges in the x and y axes that will be plotted
    if x_range is None:
        xmin, xmax = np.sort(np.vstack(z)[:, 0])[np.array([0, len(z) - 1])]
        if range_pad > 0:
            xmin -= (xmax - xmin) * range_pad
            xmax += (xmax - xmin) * range_pad
    else:
        xmin, xmax = x_range

    if y_range is None:
        ymin, ymax = np.sort(np.vstack(z)[:, 1])[np.array([0, len(z) - 1])]
        if range_pad > 0:
            ymin -= (ymax - ymin) * range_pad
            ymax += (ymax - ymin) * range_pad
    else:
        ymin, ymax = y_range

    x_block = (xmax - xmin) / column_size
    y_block = (ymax - ymin) / column_size

    # ignore segments outside of range
    z = np.array(z)
    # mask = np.array([(z[:, 0] > xmin) & (z[:, 1] > ymin) & (z[:, 0] < xmax) & (z[:, 1] < ymax)])[0]
    #
    # if "labels" in scatter_kwargs:
    #     scatter_kwargs["labels"] = np.array(scatter_kwargs["labels"])[mask]
    # specs = np.array(specs)[mask]
    # z = z[mask]

    # prepare the main axis
    main_ax = fig.add_subplot(gs[1:column_size - 1, 1:column_size - 1])
    if show_scatter:
        scatter_projections(projection=z, ax=main_ax, fig=fig, labels=labels,
                            facecolor='black',
                            show_legend=show_legend,
                            **scatter_kwargs)

    # loop through example columns
    axs = {}
    for column in range(n_columns):
        # get example column location
        if column < column_size:
            row = 0
            col = column
        elif (column >= column_size) & (column < (column_size * 2) - 1):
            row = column - column_size + 1
            col = column_size - 1
        elif (column >= ((column_size * 2) - 1)) & (column < (column_size * 3 - 2)):
            row = column_size - 1
            col = column_size - 3 - (column - column_size * 2)
        elif column >= column_size * 3 - 3:
            row = n_columns - column
            col = 0

        axs[column] = {"ax": fig.add_subplot(gs[row, col]), "col": col, "row": row}
        # label subplot
        # axs[column]["ax"].text(
        #     x=0.5,
        #     y=0.5,
        #     s=column,
        #     horizontalalignment="center",
        #     verticalalignment="center",
        #     transform=axs[column]["ax"].transAxes,
        # )

        # sample a point in z based upon the row and column
        xpos = xmin + x_block * col + x_block / 2
        ypos = ymax - y_block * row - y_block / 2
        # main_ax.text(x=xpos, y=ypos, s=column, color=pal[column])
        axs[column]["xpos"] = xpos
        axs[column]["ypos"] = ypos

    main_ax.set_xlim([xmin, xmax])
    main_ax.set_ylim([ymin, ymax])
    # This is important to avoid ending the lines at the wrong places if the two axes aren't of the same size
    main_ax.set_aspect("auto")

    # create a voronoi diagram over the x and y pos points
    points = [[axs[i]["xpos"], axs[i]["ypos"]] for i in axs.keys()]
    voronoi_kdtree = cKDTree(data=points)

    # plot voronoi
    # vor = Voronoi(points)
    # voronoi_plot_2d(vor, ax=main_ax)
    # plt.draw()

    # find where each point lies in the voronoi diagram
    if z.shape[0] < n_subset:
        z = np.random.choice(z, size=n_subset, replace=True)
    point_dist, point_regions = voronoi_kdtree.query(list(z))

    # loop through regions and select a point
    chosen_points = []
    lines_list = []
    for key in axs.keys():
        # sample a point in (or near) voronoi region
        nearest_points = np.argsort(np.abs(point_regions - key))
        possible_points = np.where(point_regions == point_regions[nearest_points][0])[0]
        chosen_point = np.random.choice(a=possible_points, size=1)[0]
        point_regions[chosen_point] = 1e4
        chosen_points.append(chosen_point)
        # plot point
        if enlarge_points > 0:
            if color_points and labels is None:
                color = pal[key]
            else:
                color = "k"
            main_ax.scatter(
                [z[chosen_point, 0]],
                [z[chosen_point, 1]],
                color=color,
                s=enlarge_points,
            )
            if hide_axes:
                main_ax.axes.get_xaxis().set_visible(False)
                main_ax.axes.get_yaxis().set_visible(False)

        # draw a line between point and image
        if draw_lines:
            line_end_xpos = xmin + (axs[key]["col"] / (column_size - 1)) * (xmax - xmin)
            line_end_ypos = ymin + (1 - axs[key]["row"] / (column_size - 1)) * (ymax - ymin)
            lines_list.append([line_end_xpos, line_end_ypos])

    if draw_lines:
        # intersect_matrix = np.zeros((len(chosen_points), len(chosen_points)), dtype="int")
        for i in range(len(chosen_points)):
            for j in range(i + 1, len(chosen_points)):
                intersect = doIntersect(z[chosen_points[i]], lines_list[i],
                                        z[chosen_points[j]], lines_list[j])
                if intersect:
                    chosen_points[i], chosen_points[j] = chosen_points[j], chosen_points[i]

        for line_end_pos, chosen_point, key in zip(lines_list, chosen_points, axs.keys()):
            if "color" not in line_kwargs.keys():
                line_kwargs['color'] = pal[key] if color_points else "k"
            main_ax.plot(
                [z[chosen_point, 0], line_end_pos[0]],
                [z[chosen_point, 1], line_end_pos[1]],
                **line_kwargs,
            )

    for chosen_point, key in zip(chosen_points, axs.keys()):
        # draw spec
        axs[key]["ax"].matshow(
            specs[chosen_point],
            origin=img_origin,
            interpolation="none",
            aspect="auto",
            **matshow_kwargs,
        )
        axs[key]["ax"].set_xticks([])
        axs[key]["ax"].set_yticks([])
        if color_points:
            plt.setp(axs[key]["ax"].spines.values(), color=pal[key])
        else:
            plt.setp(axs[key]["ax"].spines.values(), color=border_line_color)

        for i in axs[key]["ax"].spines.values():
            i.set_linewidth(border_line_width)

    gs.update(wspace=0, hspace=0)
    # gs.update(wspace=0.5, hspace=0.5)

    fig = plt.gcf()

    if ax is not None:
        buf = io.BytesIO()
        plt.savefig(buf, dpi=300, bbox_inches="tight", pad_inches=0)
        buf.seek(0)
        im = Image.open(buf)
        ax.imshow(im)
        plt.close(fig)

    return fig, axs, main_ax, [xmin, xmax, ymin, ymax]


def interactive_scatter_spec(
        z,
        specs=None,
        range_pad=0.1,
        x_range=None,
        y_range=None,
        color=None,
        matshow_kwargs={"cmap": 'inferno'},
        return_plot=False,
        info=None,
        audio=None,
        sr=48000,
        output_dir='temp'
):
    """
    Plots both embedding results and a selection of spectrograms corresponding to particular points, linking each pair
    by a dashed lined.

    z: embedding of shape (n_samples, p >= 2). Only the first two dimensions in axis p are plotted
    specs: spectrograms each corresponding to a sample in z
    """
    # Determines the ranges in the x and y axes that will be plotted
    if x_range is None and y_range is None:
        xmin, xmax = np.sort(np.vstack(z)[:, 0])[np.array([0, len(z) - 1])]
        ymin, ymax = np.sort(np.vstack(z)[:, 1])[np.array([0, len(z) - 1])]
        if range_pad > 0:
            xmin -= (xmax - xmin) * range_pad
            xmax += (xmax - xmin) * range_pad
            ymin -= (ymax - ymin) * range_pad
            ymax += (ymax - ymin) * range_pad
    else:
        xmin, xmax = x_range
        ymin, ymax = y_range

    # ignore segments outside of range
    z = np.array(z)
    data_dict = dict(x=z[:, 0], y=z[:, 1])

    # instantiate figure
    fig = figure(
        x_range=[xmin, xmax],
        y_range=[ymin, ymax],
        tools='pan,wheel_zoom,box_zoom,reset',
    )
    fig.sizing_mode = "scale_height"

    # Configure output file
    if specs is not None or audio is not None:
        output_file(os.path.join(output_dir, "main.html"))

    # invert spectrograms so that frequency axis is the correct orentiation
    tooltip = ""
    if specs is not None:
        if len(set([s.shape[1] for s in specs])) <= 1:
            specs = [s.T for s in specs]
        specs = [s[::-1] for s in specs]
        # convert to uint8 type to reduce memory footprint
        specs = [(255 * s).astype('uint8') for s in specs]

        _write_images(specs, output_dir=output_dir, num_imgs=-1)
        print('Finished writing images')
        data_dict.update(dict(imgs=[f'./{str(i)}.jpg' for i in range(len(z))]))

        # Prepare tooltips
        tooltip += """
            <div>
                <div>
                    <img
                        src="@imgs" height="100" alt="@imgs" width="100"
                        style="float: left; margin: 0px 0px 0px 0px;"
                        border="2"
                    ></img>
                </div>
            """

    if info is not None and isinstance(info, pd.DataFrame):
        data_dict.update(**{k: info[k].tolist() for k in info.columns})
        for col in list(info.columns):
            tooltip += f"<div> <span>{col} @{col}</span> </div>"

    if color is not None:
        data_dict.update(dict(color=color))
        tooltip += "<div> <span>@color</span> </div>"
        try:
            unique_values = list(set(color))
            if len(unique_values) > 20:
                colormapper = linear_cmap(field_name='color', palette="Viridis256", low=float(min(color)),
                                          high=float(max(color)))
            else:
                colormapper = factor_cmap(field_name='color', palette=Category20[min(20, len(unique_values))],
                                          factors=unique_values)
        except (TypeError, ValueError):
            colormapper = linear_cmap(field_name='color', palette="Viridis256", low=float(min(color)),
                                      high=float(max(color)))

    tooltip += "</div>"
    fig.add_tools(HoverTool(tooltips=tooltip))

    data_source = ColumnDataSource(data=data_dict)

    if audio is not None:
        data_source.data['audio'] = [f'./{str(i)}.wav' for i in range(len(z))]
        _write_sounds(audio, output_dir=output_dir, sr=sr, num_sounds=-1)
        print('Finished writing sounds')
        fig.js_on_event('tap',
                        CustomJS(
                            args=dict(source=data_source),
                            code="""
                            // coordinates of cursort
                            var coord_x = cb_obj.x;
                            var coord_y = cb_obj.y;

                            // find the closest point (in Euclidean space)
                            var index = 0;
                            var min_dist = Math.pow(coord_x - source.data['x'][0], 2) + Math.pow(coord_y - source.data['y'][0], 2);
                            for (var j = 1; j < source.data['audio'].length; j++){
                                var dist = Math.pow(coord_x - source.data['x'][j], 2) + Math.pow(coord_y - source.data['y'][j], 2);
                                if (dist < min_dist){
                                    index = j;
                                    min_dist = dist;
                                }
                            }
                            console.log(source.data);
                            var audio = new Audio(source.data['audio'][index]);
                            audio.play();                            
                            """
                        ))
        # data_dict.update({'audio': audio})
        # fig.js_on_event('tap', CustomJS(args=dict(source=data_source),
        #                                 code="""
        #                                    var sr = %d;
        #                                    // coordinates of cursort
        #                                    var coord_x = cb_obj.x;
        #                                    var coord_y = cb_obj.y;
        #
        #                                    // find the closest point (in Euclidean space)
        #                                    var index = 0;
        #                                    var min_dist = Math.pow(coord_x - source.data['x'][0], 2) + Math.pow(coord_y - source.data['y'][0], 2);
        #                                    for (var j = 1; j < source.data['audio'].length; j++){
        #                                         var dist = Math.pow(coord_x - source.data['x'][j], 2) + Math.pow(coord_y - source.data['y'][j], 2);
        #                                         if (dist < min_dist){
        #                                             index = j;
        #                                             min_dist = dist;
        #                                         }
        #                                    }
        #
        #                                    var currentChannel = source.data['audio'][index];
        #                                    var size = currentChannel.length;
        #
        #                                    var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        #                                    var myArrayBuffer = audioCtx.createBuffer(1, size, sr);
        #                                    var nowBuffering = myArrayBuffer.getChannelData(0);
        #                                    for (var i = 0; i < size; i++) {
        #                                      nowBuffering[i] = currentChannel[i];
        #                                     }
        #
        #                                     var playSource = audioCtx.createBufferSource();
        #                                     playSource.buffer = myArrayBuffer;
        #                                     playSource.connect(audioCtx.destination);
        #                                     playSource.start();
        #                                     """ % sr))

    # prepare the main axis
    if color is not None:
        fig.scatter(x="x",
                    y="y",
                    color=colormapper,
                    # color={'field_name': 'color', 'transform': color_coding},
                    source=data_source)
    else:
        fig.scatter(x="x",
                    y="y",
                    source=data_source)

    if return_plot:
        return fig
    else:
        show(fig)


def scatter_spec(projection,
                 specs=None,
                 interactive=False,
                 range_pad=0.1,
                 x_range=None,
                 y_range=None,
                 color=None,
                 matshow_kwargs={"cmap": 'inferno'},
                 scatter_kwargs={"alpha": 0.5, "s": 1},
                 line_kwargs={"lw": .5, "ls": "dashed", "alpha": 1, 'color': 'white'},
                 column_size=10,
                 pal_color="hls",
                 color_points=False,
                 figsize=(10, 10),
                 enlarge_points=0,
                 draw_lines=True,
                 n_subset=-1,
                 ax=None,
                 show_scatter=True,
                 show_legend=True,
                 border_line_width=1,
                 img_origin="lower",
                 info=None,
                 audio=None,
                 sr=48000,
                 # return_plot=False,
                 ):
    """
    Very thin wrapper aroung scatter_spec and interactive_scatter_spec.
    Exposes the arguments of both, but which are actually taken depends on the value of the interactive argument.

    If interactive=True, returns an interactive plot showing the spectrograms corresponding to each point of z
    """
    if interactive:
        interactive_scatter_spec(z=projection,
                                 specs=specs,
                                 range_pad=range_pad,
                                 x_range=x_range,
                                 y_range=y_range,
                                 color=color,
                                 matshow_kwargs=matshow_kwargs,
                                 return_plot=False,
                                 info=info,
                                 audio=audio,
                                 sr=sr
                                 )
    else:
        static_scatter_spec(z=projection,
                            specs=specs,
                            labels=color,
                            column_size=column_size,
                            pal_color=pal_color,
                            matshow_kwargs=matshow_kwargs,
                            scatter_kwargs=scatter_kwargs,
                            line_kwargs=line_kwargs,
                            color_points=color_points,
                            figsize=figsize,
                            range_pad=range_pad,
                            x_range=x_range,
                            y_range=y_range,
                            enlarge_points=enlarge_points,
                            draw_lines=draw_lines,
                            n_subset=n_subset,
                            ax=ax,
                            show_scatter=show_scatter,
                            border_line_width=border_line_width,
                            img_origin=img_origin,
                            show_legend=show_legend
                            # return_plot=return_plot
                            )


colormap_list: list = ["nipy_spectral",
                       "terrain",
                       "tab20b",
                       "tab20c",
                       "gist_rainbow",
                       "hsv",
                       "CMRmap",
                       "coolwarm",
                       "gnuplot",
                       "gist_stern",
                       "brg",
                       "rainbow",
                       "jet"]
hatch_list: list = ['//', '\\\\', '||', '--', '++', 'xx', 'oo', 'OO', '..', '**', '/o', '\\|', '|*', '-\\', '+o', 'x*',
                    'o-', 'O|', 'O.', '*-']
maker_list: list = ['.', '_', '+', '|', 'x', 'v', '^', '<', '>', 's', 'p', '*', 'h', 'D', 'd', 'P', 'X', 'o', '1', '2',
                    '3', '4', '|', '_']


def _radialtree2(Z2,
                 fontsize: int = 8,
                 figsize: Optional[list] = None,
                 addlabels: bool = False,
                 sample_classes: Optional[dict] = None,
                 colorlabels: Optional[dict] = None,
                 ax: Optional[plt.Axes] = None,
                 linewidth: float = 1,
                 showlegend: bool=True,
                 label_palettes: List = colormap_list,
                 ) -> plt.Axes:
    """
    Drawing a radial dendrogram from a scipy dendrogram output.
    Parameters
    ----------
    Z2 : dictionary
        A dictionary returned by scipy.cluster.hierarchy.dendrogram
    addlabels: bool
        A bool to choose if labels are shown.
    fontsize : float
        A float to specify the font size
    figsize : [x, y] array-like
        1D array-like of floats to specify the figure size
    palette : string
        Matlab colormap name.
    sample_classes : dict
        A dictionary that contains lists of sample subtypes or classes. These classes appear
        as color labels of each leaf. Colormaps are automatically assigned. Not compatible
        with options "colorlabels" and "colorlabels_legend".
        e.g., {"color1":["Class1","Class2","Class1","Class3", ....]}
    colorlabels : dict
        A dictionary to set color labels to leaves. The key is the name of the color label.
        The value is the list of RGB color codes, each corresponds to the color of a leaf.
        e.g., {"color1":[[1,0,0,1], ....]}
    colorlabels_legend : dict
        A nested dictionary to generate the legends of color labels. The key is the name of
        the color label. The value is a dictionary that has two keys "colors" and "labels".
        The value of "colors" is the list of RGB color codes, each corresponds to the class of a leaf.
        e.g., {"color1":{"colors":[[1,0,0,1], ....], "labels":["label1","label2",...]}}
    show : bool
        Whether or not to show the figure.
    """
    R = 1
    width = R * 0.1
    space = R * 0.05

    if colorlabels is not None:
        offset = width * len(colorlabels) / R + space * (len(colorlabels) - 1) / R + 0.05
    elif sample_classes is not None:
        offset = width * len(sample_classes) / R + space * (len(sample_classes) - 1) / R + 0.05
    else:
        offset = 0

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)

    xmax = np.amax(Z2['icoord'])
    ymax = np.amax(Z2['dcoord'])

    # the rightmost leaf is going to [1, 0]
    # transforming original x and y coordinates into circumference and radius positions
    coord = np.array(Z2['icoord'])
    coord *= 2 * np.pi / xmax
    r = np.array(Z2['dcoord'])
    r = R * (1 - r / ymax)
    xr = r * np.cos(coord)
    yr = r * np.sin(coord)

    # plotting radial lines as a LineCollection
    radial_segments = np.concatenate(
        [np.stack([xr[:, :2], yr[:, :2]], axis=-1),
         np.stack([xr[:, 2:], yr[:, 2:]], axis=-1)],
        axis=0
    )
    radial_segments = LineCollection(radial_segments,
                                     colors=["black"] * len(radial_segments),
                                     linewidth=linewidth,
                                     rasterized=True)
    ax.add_collection(radial_segments)

    # plot circular links between nodes as a LineCollection
    # _lineres = 1000
    # xinterval = np.array(Z2['icoord'])
    # xinterval = np.abs(xinterval[:, 0] - xinterval[:, 2]) / xmax
    # lineres = np.clip((_lineres * xinterval).astype('int'), a_min=10, a_max=100)
    lineres = 100
    circular_segments = np.zeros(shape=(2 * xr.shape[0], lineres, 2))
    k = 0
    for i, (_xr, _yr, _r) in enumerate(zip(xr, yr, r)):
        if _yr[1] > 0 and _yr[2] > 0:
            link = np.linspace(_xr[1], _xr[2], lineres)
            circular_segments[k, :, 0] = link
            circular_segments[k, :, 1] = np.sqrt(_r[1] ** 2 - link ** 2)

        elif _yr[1] < 0 and _yr[2] < 0:
            link = np.linspace(_xr[1], _xr[2], lineres)
            circular_segments[k, :, 0] = link
            circular_segments[k, :, 1] = -np.sqrt(_r[1] ** 2 - link ** 2)

        elif _yr[1] > 0 and _yr[2] < 0:
            # this particular case is for segments that include the (-1, 0) point
            # we need to split them in two, one above and one below (-1, 0)
            if _xr[1] < 0 or _xr[2] < 0:
                _rr = -_r[1]
            else:
                _rr = _r[1]

            link = np.linspace(_xr[1], _rr, lineres)
            circular_segments[k, :, 0] = link
            circular_segments[k, :, 1] = np.sqrt(_r[1] ** 2 - link ** 2)
            k += 1

            link = np.linspace(_rr, _xr[2], lineres)
            circular_segments[k, :, 0] = link
            circular_segments[k, :, 1] = -np.sqrt(_r[1] ** 2 - link ** 2)
        k += 1

    circular_segments = LineCollection(circular_segments[:k],
                                       colors=['black'] * k,
                                       linewidth=linewidth,
                                       rasterized=True)
    ax.add_collection(circular_segments)

    # determine the coordinates of the labels and their rotation:
    place = (5. + 10. * np.arange(len(Z2['ivl']))) / xmax * 2.
    label_coords = np.array([
        np.cos(place * np.pi) * (1.05 + offset),  # _x
        np.sin(place * np.pi) * (1.05 + offset),  # _y
        place * 180,  # _rot
    ])

    if addlabels:
        # Adding labels
        for (_x, _y, _rot), label in zip(label_coords.T, Z2['ivl'], strict=True):
            ax.text(_x,
                    _y,
                    label,
                    {'va': 'center'},
                    rotation_mode='anchor',
                    rotation=_rot,
                    fontsize=fontsize)

    if sample_classes is not None:
        outerrad = R * 1.05 + width * len(sample_classes) + space * (len(sample_classes) - 1)

        labelnames = []
        colorlabels_legend = {}
        for classcounter, (labelname, colorlist) in enumerate(sample_classes.items()):
            ucolors = sorted(list(np.unique(colorlist)))
            type_num = len(ucolors)
            _cmp = plt.get_cmap(label_palettes[classcounter], len(colorlist)+2)
            _colorlist = [_cmp(ucolors.index(c) / max(1, (type_num - 1))) for c in colorlist]
            _colorlist = np.array(_colorlist)[Z2['leaves']]
            if classcounter != 0:
                outerrad = outerrad - width - space
            innerrad = outerrad - width

            _x, _y, _rot = label_coords
            _xl, _yl, _rotl = np.roll(label_coords, axis=-1, shift=1)
            _xr, _yr, _rotr = np.roll(label_coords, axis=-1, shift=-1)
            # roll over the rotations
            _rotl[0] -= 360
            _rotr[-1] += 360

            start = np.pi / 360 * (_rot + _rotl)
            theta = np.pi / 360 * (_rotr - _rotl)

            stth = start + theta
            move = np.linspace(start, stth, 10)

            def coord_transform(transform):
                mov = transform(move)
                stt = transform(stth)
                star = transform(start)
                return np.concatenate([
                    innerrad * mov,
                    [innerrad * stt, outerrad * stt],
                    outerrad * mov[::-1],
                    [innerrad * star, outerrad * star][::-1]
                ]).T

            xf = coord_transform(transform=np.cos)
            yf = coord_transform(transform=np.sin)
            polycol = np.stack([xf, yf], axis=-1)
            polycol = PolyCollection(polycol, facecolors=_colorlist, linewidths=0)
            ax.add_collection(polycol)

            labelnames.append(labelname)
            colorlabels_legend[labelname] = {
                'colors': _cmp(np.linspace(0, 1, type_num)),
                'labels': ucolors
            }

        if colorlabels_legend is not None and showlegend:
            for i, labelname in enumerate(labelnames):
                leg = plt.legend([Line2D([0], [0], color=c, lw=4) for c in colorlabels_legend[labelname]["colors"]],
                                 colorlabels_legend[labelname]["labels"],
                                 bbox_to_anchor=(1. + .1 * i, 1. - .3 * i),
                                 loc='center left',
                                 title=labelname)
                plt.gca().add_artist(leg)

    ax.spines.right.set_visible(False)
    ax.spines.top.set_visible(False)
    ax.spines.left.set_visible(False)
    ax.spines.bottom.set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])

    if colorlabels is not None:
        maxr = R * 1.1 + width * len(colorlabels) + space * (len(colorlabels) - 1)
    elif sample_classes is not None:
        maxr = R * 1.1 + width * len(sample_classes) + space * (len(sample_classes) - 1)
    else:
        maxr = R * 1.1
    ax.set_xlim(-maxr, maxr)
    ax.set_ylim(-maxr, maxr)
    # plt.subplots_adjust(left=0.05, right=0.75)

    return ax


def radialtree(
        X,
        Y: Optional = None,
        sample_names: Optional[str] = None,
        ztransform: bool = False,
        metric="euclidean",
        tree_method="ward",
        title: str = "",
        linewidth: float = 1,
        figsize: Optional[list | tuple] = None,
        ax: Optional = None,
        label_palettes: List = colormap_list,
        optimal_ordering: bool = False,
        **kwargs
) -> Dict:
    """
    Draw a radial dendrogram with color labels.

    Example: Assuming df is a pandas DataFrame with the correct columns:
    radialtree(
        X=df[['Embedding_V0', 'Embedding_V1']],
        Y=df[['Sex', 'Song', 'Colony', 'Source']],
        label_palettes=['Set1', 'tab10', 'Set3', 'nipy_spectral']
    )
    The radial dendrogram is drawn from the coordinates provided by X (either as an array of shape (n_samples, n_dim) or
    as a distance matrix of shape (n_samples, n_samples)), then add circles of coloured areas corresponding to each
    column in Y.

    X and Y must be pandas DataFrame
    label_palettes must be either a list of matplotlib color palettes (one per column of Y) or a single palette (to use
    the same for all columns of Y).
    metric argument can be used to specify a metric if X is not already a distance matrix

    """
    if ztransform and X.ndim == 2:
        X = zscore(X, axis=0)

    L = sch.linkage(X, method=tree_method, metric=metric, optimal_ordering=optimal_ordering)

    if sample_names is None:
        if isinstance(X, pd.DataFrame):
            sample_names = X.index
        else:
            sample_names = np.arange(L.shape[0] + 1)

    Z = sch.dendrogram(L, labels=sample_names, no_plot=True)
    try:
        sample_classes = {k: list(Y[k]) for k in Y.columns}
    except AttributeError:
        # if Y is not a pd.DataFrame object
        sample_classes = {"": Y}

    ax = _radialtree2(Z,
                      sample_classes=sample_classes,
                      linewidth=linewidth,
                      figsize=figsize,
                      ax=ax,
                      label_palettes=label_palettes,
                      **kwargs)
    if title != "":
        ax.set_title(title)

    return ax


if __name__ == '__main__':
    from sklearn.datasets import make_blobs

    n = 5000
    test, _ = make_blobs(n_samples=n, n_features=2, centers=3)
    specs = [np.random.uniform(size=(40, 40)) for i in range(n)]
    audio = [np.random.uniform(size=(48000,), low=-.5, high=.5) for i in range(n)]
    scatter_spec(
        projection=test,
        specs=specs,
        audio=audio,
        interactive=True,
    )