from sklearn.neighbors import BallTree, NearestNeighbors
from sklearn.metrics.pairwise import pairwise_distances
from umap import UMAP, plot as uplt
from umap.umap_ import nearest_neighbors
import pacmap
import matplotlib.pyplot as plt
from warnings import warn
import numpy as np
import pandas as pd
from typing import Union
import itertools
from numba import njit, prange

from .utilities import product_dict, restrict_kwargs

from sklearn.neighbors import NearestNeighbors
from math import isnan
from scipy.spatial import Delaunay


def in_hull(p, hull):
    """
    Test if points in `p` are in `hull`
    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    if not isinstance(hull, Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p) >= 0


@njit
def sample_from_hull(hull, xmin, xmax, dims, n):
    samples = np.empty(shape=(n, dims))
    for _ in prange(n):
        in_hull_bool = False
        while not in_hull_bool:
            samp = np.random.uniform(xmin, xmax, dims)
            in_hull_bool = in_hull(samp, hull)
        samples[_] = samp
    return samples


def hopkins_statistic(X, m_prop_n=0.1, n_neighbors=1, distribution="uniform", flip=False):
    """ Computes hopkins statistic over a distribution X
    based upon:
    - https://matevzkunaver.wordpress.com/2017/06/20/hopkins-test-for-cluster-tendency/
    - https://github.com/rflachlan/Luscinia/wiki/Hopkins-statistic
    - https://en.wikipedia.org/wiki/Hopkins_statistic
    - https://pypi.org/project/pyclustertend/
    -
    X: The original dataset
    m: number of samples in Y
    n: number of samples in X
    dims: number of dimensions in x
    nbrs: nearest neighbor for each value of X
    ujd: the distance of y_i from its nearest neighbor in X
    wjd: the distance of x_i from its nearest neighbor in X

    flip: flags which of two possible definitions of the Hopkins statistic to use:
        let u be the distance between a real point x and its nearest neighbour in X
        let w be the distance a simulated point y and its nearest neighbour in X
        False: H = u / (u + w) -> H should be close to 1 for clusterable data
        True: H = w / (u + w) -> H should be close to 0 for clusterable data
        Note that H(flip=True) + H(flip=False) = 1 in any case.

    """
    # convert to pandas dataframe
    if type(X) == np.ndarray:
        X = pd.DataFrame(X)

    n_samples, n_dims = X.shape

    # the nearest neigbor(s) for each element in X
    nbrs = NearestNeighbors(n_neighbors=1).fit(X.values)

    # statistics over X (for sampling)
    xmin = X.min()
    xmax = X.max()
    loc = X.mean()
    scale = X.std()

    # choose how many samples over X
    m = int(np.ceil(m_prop_n * n_samples))

    # sample from X m times
    rand_X = np.random.choice(n_samples, m, replace=False)

    if distribution == "uniform_convex_hull":
        hull = Delaunay(X)

    def sample_dist(n):
        if distribution == "uniform":
            return np.random.uniform(xmin, xmax, (n, n_dims))
        elif distribution == "normal":
            return np.random.normal(loc, scale, (n, n_dims))
        elif distribution == "uniform_convex_hull":
            return sample_from_hull(hull, xmin, xmax, n_dims, n)
        else:
            raise ValueError(
                'distribution must be "uniform", "uniform_convex_hull", or "normal"'
            )

    # sample Y (since its with replacement, repeat each time)
    y = sample_dist(m)

    # distance of each sample of y from its nearest neighbor in X
    u_dist, _ = nbrs.kneighbors(y, n_neighbors + 1, return_distance=True)
    ujd = np.mean(u_dist[:, 1:], axis=1)

    # get the distance from X sample to the nearest neighbors in X
    w_dist, _ = nbrs.kneighbors(X.iloc[rand_X], n_neighbors + 1, return_distance=True)
    wjd = np.mean(w_dist[:, 1:], axis=1)

    if flip:
        # distances of true distributions / distances of sampled + distances of true distribution
        # i.e. H should be close to 1 for clusterable data
        H = sum(wjd) / (sum(ujd) + sum(wjd))
    else:
        # distances of sampled / distances of sampled + distances of true distribution
        # i.e. H should be close to 0 for clusterable data
        H = sum(ujd) / (sum(ujd) + sum(wjd))

    if isnan(H):
        print(ujd, wjd)
        H = 0

    return H


def _is_distance_matrix(X):
    errors = []
    if X.ndim != 2:
        errors.append(f'{X.ndim}D array')
    if X.shape[0] != X.shape[1]:
        errors.append('not square')
    if not np.all(X == X.T):
        errors.append('not symmetrical')
    if np.sum(np.diagonal(X)) > 0:
        errors.append('non-zero diagonal')

    return errors


def pacmap_fit_transform(X, n_neighbors=15, n_components=2, metric='precomputed', **kwargs):
    from time import time
    if "y" in kwargs:
        kwargs.pop('y')

    n = X.shape[0]

    knn = NearestNeighbors(n_neighbors=n_neighbors, metric=metric).fit(X)
    _, nbrs = knn.kneighbors()

    scaled_dist = np.ones((n, n_neighbors), dtype=np.float32)  # No scaling is needed

    # Type casting is needed for numba acceleration
    X = X.astype(np.float32)
    nbrs = nbrs.astype(np.int32)

    pair_neighbors = pacmap.sample_neighbors_pair(X, scaled_dist, nbrs, np.int32(n_neighbors))

    # initializing the pacmap instance
    # feed the pair_neighbors into the instance
    embedding = pacmap.PaCMAP(n_components=n_components,
                              n_neighbors=n_neighbors,
                              pair_neighbors=pair_neighbors,
                              distance='euclidean' if metric == 'precomputed' else metric,
                              **kwargs)

    # fit the data (The index of transformed data corresponds to the index of the original data)
    X_transformed = embedding.fit_transform(X, init="pca")

    return X_transformed


def multiple_embeddings(X, y=None, test_parameters=None, suppress_warn=False, recycle_knn=True, random_state=None, kind='umap', **kwargs):
    if test_parameters is None:
        raise ValueError('No test_parameters were provided, please provided some as a dict object.'
                         'The dict keys should have the same names as the corresponding UMAP arguments, and the'
                         'dict values should be iterables of scalars.'
                         'Note: only parameters that should vary between runs need to be passed to test_parameters.'
                         'Parameters shared between runs can be passed as named arguments to the function.')

    if 'metric' not in kwargs and 'metric' not in test_parameters:
        errors = _is_distance_matrix(X)
        if X.shape[0] == X.shape[1] and len(errors) == 0:
            kwargs['metric'] = 'precomputed'
        elif not suppress_warn:
            warn(f'No "metric" argument provided and X is not apparently NOT a distance matrix, '
                 f'as the following properties are missing: {", ".join(errors)}. '
                 f'We are therefore using the default "euclidean" metric for computation.'
                 f'If this is desired behaviour, you may silence this warning by setting suppress_warn=True.')

    # get the highest values first
    # in particular, the largest n_neighbors value should be done first to compute the knn_graph
    test_parameters = {k: list(reversed(sorted(v))) for k, v in test_parameters.items()}

    if 'kind' not in test_parameters:
        test_parameters['kind'] = [kind]
    test_parameters['kind'] = [_.lower() for _ in test_parameters['kind']]

    if recycle_knn:
        if 'n_neighbors' in test_parameters:
            max_k = max(test_parameters['n_neighbors'])
        else:
            max_k = max(kwargs['n_neighbors'])
        knn_graph = nearest_neighbors(X,
                                      n_neighbors=max_k,
                                      metric=kwargs['metric'],
                                      metric_kwds=kwargs['metric_kwds'] if 'metric_kwds' in kwargs else None,
                                      angular=False,
                                      random_state=random_state)
    else:
        knn_graph = (None, None, None)

    # Create all combinations
    test_parameters = product_dict(**test_parameters)

    umap_kwargs = restrict_kwargs(UMAP, kwargs)
    pacmap_kwargs = restrict_kwargs(pacmap_fit_transform, kwargs)

    outs = []
    from time import time
    for params in test_parameters:
        kind = params.pop('kind')
        if kind.lower() == 'umap':
            embedding = UMAP(precomputed_knn=knn_graph,
                             random_state=random_state,
                             **params,
                             **umap_kwargs
                             ).fit_transform(X, y=y)
        elif kind.lower() == 'pacmap':
            embedding = pacmap_fit_transform(X,
                                             random_state=random_state,
                                             **params,
                                             **pacmap_kwargs
                                             )
        else:
            raise ValueError(f'{kind=} value is not understood, must be either equal or coercible to "umap" or "pacmap" with str.lower().')

        outs.append({**params, 'kind': kind, "embedding": embedding})

    # converts the list of dicts to a dict of lists
    outs = {k: [dc[k] for dc in outs] for k in outs[0]}

    return outs


def trustworthiness(X, X_embedded, *, n_neighbors=5, input_metric='precomputed', output_metric='euclidean'):
    r"""
    Lifted almost straight from sklearn.manifold.trustworthiness, but adjusted to facilitate computations on a list
    of embeddings without redoing argsort(X) to get the nearest neighbours.

    Also a minor modification to avoid integer overflow
    """
    if not isinstance(X_embedded, list):
        X_embedded = [X_embedded]
    if not isinstance(n_neighbors, list):
        n_neighbors = [n_neighbors]

    if 1 != len(n_neighbors) and len(n_neighbors) != len(X_embedded):
        raise ValueError(f'X_embbedded is a list of length {len(X_embedded)} but n_neighbors is a list of '
                         f'length {len(n_neighbors)}, please provide two lists of the same length.')

    n_samples = len(X)

    if input_metric != 'precomputed':
        X = pairwise_distances(X, metric=input_metric)

    # we set the diagonal to np.inf to exclude the points themselves from
    # their own neighborhood
    np.fill_diagonal(X, np.inf)
    ind_X = np.argsort(X, axis=1)

    # We build an inverted index of neighbors in the input space: For sample i,
    # we define `inverted_index[i]` as the inverted index of sorted distances:
    # inverted_index[i][ind_X[i]] = np.arange(1, n_sample + 1)
    # initiate to np.int64 instead of plain int to limit integer overflow
    inverted_index = np.zeros((n_samples, n_samples), dtype=np.int64)
    ordered_indices = np.arange(n_samples + 1)
    inverted_index[ordered_indices[:-1, np.newaxis], ind_X] = ordered_indices[1:]

    def _compute_trustworthiness(embedded_X, k):
        # `ind_X[i]` is the index of sorted distances between i and other samples
        ind_X_embedded = NearestNeighbors(n_neighbors=k, metric=output_metric).fit(embedded_X).kneighbors(return_distance=False)
        ranks = inverted_index[ordered_indices[:-1, np.newaxis], ind_X_embedded] - k
        t = np.sum(ranks[ranks > 0])
        K = 2. / (n_samples * k * (2. * n_samples - 3. * k - 1.))
        t = 1. - t * K
        return t

    trust = np.array([_compute_trustworthiness(x, n) for x, n in zip(X_embedded, itertools.cycle(n_neighbors))])

    return trust


def continuity(X, X_embedded, *, n_neighbors=5, input_metric='precomputed', output_metric = 'euclidean'):
    r"""
    Lifted almost straight from sklearn.manifold.trustworthiness, but adjusted to facilitate computations on a list
    of embeddings without redoing argsort(X) to get the nearest neighbours.

    Also a minor modification to avoid integer overflow
    """
    if not isinstance(X_embedded, list):
        X_embedded = [X_embedded]
    if not isinstance(n_neighbors, list):
        n_neighbors = [n_neighbors]

    if 1 != len(n_neighbors) and len(n_neighbors) != len(X_embedded):
        raise ValueError(f'X_embbedded is a list of length {len(X_embedded)} but n_neighbors is a list of '
                         f'length {len(n_neighbors)}, please provide two lists of the same length (or a scalar value for n_neighbors')

    n_samples = len(X)

    if input_metric != 'precomputed':
        X = pairwise_distances(X, metric=input_metric)

    # we set the diagonal to np.inf to exclude the points themselves from
    # their own neighborhood
    np.fill_diagonal(X, np.inf)
    ind_X = np.argsort(X, axis=1)

    # We build an inverted index of neighbors in the input space: For sample i,
    # we define `inverted_index[i]` as the inverted index of sorted distances:
    # inverted_index[i][ind_X[i]] = np.arange(1, n_sample + 1)
    # initiate to np.int64 instead of plain int to limit integer overflow
    inverted_index = np.zeros((n_samples, n_samples), dtype=np.int64)
    ordered_indices = np.arange(n_samples + 1)
    inverted_index[ordered_indices[:-1, np.newaxis], ind_X] = ordered_indices[1:]

    def _compute_continuity(embedded_X, k):
        if output_metric != 'precomputed':
            embedded_X = pairwise_distances(embedded_X, metric=output_metric)

        np.fill_diagonal(embedded_X, np.inf)
        ind_X_embedded = np.argsort(embedded_X, axis=1)

        inverted_index = np.zeros((n_samples, n_samples), dtype=np.int64)
        ordered_indices = np.arange(n_samples + 1)
        inverted_index[ordered_indices[:-1, np.newaxis], ind_X_embedded] = ordered_indices[1:]

        ranks = inverted_index[ordered_indices[:-1, np.newaxis], ind_X[:, :k]] - k
        t = np.sum(ranks[ranks > 0])
        K = 2. / (n_samples * k * (2. * n_samples - 3. * k - 1.))
        c = 1. - t * K
        return c

    cont = np.array([_compute_continuity(x, n) for x, n in zip(X_embedded, itertools.cycle(n_neighbors))])

    return cont


def continuity_trustworthiness(X, X_embedded, *, n_neighbors=5, input_metric='precomputed', output_metric = 'euclidean'):
    r"""
    Lifted almost straight from sklearn.manifold.trustworthiness, but adjusted to facilitate computations on a list
    of embeddings without redoing argsort(X) to get the nearest neighbours.

    Also a minor modification to avoid integer overflow
    """
    if not isinstance(X_embedded, list):
        X_embedded = [X_embedded]
    if not isinstance(n_neighbors, list):
        n_neighbors = [n_neighbors]

    if 1 != len(n_neighbors) and len(n_neighbors) != len(X_embedded):
        raise ValueError(f'X_embbedded is a list of length {len(X_embedded)} but n_neighbors is a list of '
                         f'length {len(n_neighbors)}, please provide two lists of the same length (or a scalar value for n_neighbors')

    n_samples = len(X)

    if input_metric != 'precomputed':
        X = pairwise_distances(X, metric=input_metric)

    # we set the diagonal to np.inf to exclude the points themselves from
    # their own neighborhood
    np.fill_diagonal(X, np.inf)
    ind_X = np.argsort(X, axis=1)

    # We build an inverted index of neighbors in the input space: For sample i,
    # we define `inverted_index[i]` as the inverted index of sorted distances:
    # inverted_index[i][ind_X[i]] = np.arange(1, n_sample + 1)
    # initiate to np.int64 instead of plain int to limit integer overflow
    inverted_index_X = np.zeros((n_samples, n_samples), dtype=np.int64)
    ordered_indices = np.arange(n_samples + 1)
    inverted_index_X[ordered_indices[:-1, np.newaxis], ind_X] = ordered_indices[1:]

    def _compute(embedded_X, k):
        if output_metric != 'precomputed':
            embedded_X = pairwise_distances(embedded_X, metric=output_metric)

        np.fill_diagonal(embedded_X, np.inf)
        ind_X_embedded = np.argsort(embedded_X, axis=1)
        inverted_index_embedded = np.zeros((n_samples, n_samples), dtype=np.int64)
        # ordered_indices = np.arange(n_samples + 1)
        inverted_index_embedded[ordered_indices[:-1, np.newaxis], ind_X_embedded] = ordered_indices[1:]

        K = 2. / (n_samples * k * (2. * n_samples - 3. * k - 1.))

        ranks = inverted_index_embedded[ordered_indices[:-1, np.newaxis], ind_X[:, :k]] - k
        c = np.sum(ranks[ranks > 0])
        c = 1. - c * K

        ranks = inverted_index_X[ordered_indices[:-1, np.newaxis], ind_X_embedded[:, :k]] - k
        t = np.sum(ranks[ranks > 0])
        t = 1. - t * K
        return t, c

    res = np.array(list(zip(*[_compute(x, n) for x, n in zip(X_embedded, itertools.cycle(n_neighbors))])))
    return res


# def diagnostic(data, n, filename=None):
#     embedding = UMAP(
#         n_neighbors=n,
#         n_components=2,
#         min_dist=0.,
#         metric='precomputed',
#         random_state=42
#     ).fit(data)
#
#     clusters = HDBSCAN(
#         min_cluster_size=n,
#         metric='euclidean',
#         prediction_data=True,
#     ).fit(embedding.embedding_)
#
#     predicted_clusters = clusters.labels_
#     soft_clusters = all_points_membership_vectors(clusters)
#     clustering_entropy = - np.sum(soft_clusters * np.log2(soft_clusters), axis=-1)
#     typicality = np.diff(np.sort(soft_clusters, axis=-1), axis=-1)[:, -1]
#
#     fig, axs = plt.subplots(nrows=2, ncols=4)
#     uplt.points(embedding, labels=predicted_clusters, ax=axs[0, 0], show_legend=False)
#     uplt.points(embedding, values=1-clustering_entropy, ax=axs[0, 1])
#     uplt.points(embedding, values=typicality, ax=axs[1, 0])
#     uplt.points(embedding, values=clusters.outlier_scores_, ax=axs[1, 1])
#     uplt.diagnostic(embedding, diagnostic_type='pca', ax=axs[0, 2])
#     # uplt.diagnostic(embedding, diagnostic_type='neighborhood', ax=axs[1, 2])
#     # uplt.diagnostic(embedding, diagnostic_type='local_dim', ax=axs[1, 2])
#     axs[0, 3].hist(typicality, alpha=1)
#     axs[1, 3].hist(clustering_entropy, alpha=1)
#
#     if filename is not None:
#         plt.savefig(filename)
#     else:
#         plt.show()

