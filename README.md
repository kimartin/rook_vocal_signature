# rook_vocal_signature

This repo hosts code for our paper 'Can we trace the social affiliation of rooks (\textit{Corvus frugilegus}) through their vocal signature?' (LINK INCOMING)
The corresponding data is hosted on Zenodo at https://zenodo.org/records/10580110.

The 'function' and 'scripts' folders contain the Python code used for analysis, including: extracting vocalisations, denoising, computing spectrograms, pairwise acoustic distance using the Dynamic Frequency-Time Warping distance, and saving the results


## How to run

- First download the dataset at the Zenodo link above, which should have the following organisation (after unzipping):
    - "tableau_vocs.tsv" file contains all the necessary info to extract the vocalisations, including filenames and timestamps, as well as the identity of the individual rook producing the vocalisation, the colony and the nest cluster/subgroup it belongs to.
    - 5 folders (one per colony recorded in the study) containing the audio (as .flac files), as well as the original .txt labels (which can be useful for reviewing the data in e.g. Audacity by importing a .flac audio and the correspond .txt file).

- Starting with the config.py script, change the variables under "Path to the data"
    - label_data should be the path leading to the tableau_vocs.tsv file
    - audio_path should lead to the directory the 5 folders of audio are in 
    - data_path and cluster_path can be left as None or changed, they determine where to save the extracted vocalisations and acoustic distances respectively
    - Change any other variable if necessary (the default values are the ones used for the article)

- Run, in this order and with no edits necessary:
    - dataset.py
    - spectrogramming.py
        (commenting the call to plot_and_play_channel is possible but does not block exectution or take up much time)
    - distance_matrics.py
    - knn_stats.py
This will result in saving the vocalisations and the acoustic distances at the paths specified above, and then computing the kNN analysis described in the article (in brief, we compute the kNN accuracy when predicting individual, nest cluster and colony at various values of k, with and without controlling for the individual; controlling for the individual means discarding calls from the same individual when computing accuracy for nest cluster and colony, and taking the k nearest neighbouring calls that are from different individuals)

- embed.py, projection.py and choose_umap_parameters.py are scripts used for visualisation with the UMAP algorithm and are not necessary for replicating the analysis.

## Replicating statistical analysis and figures

The statistical analysis and figures were computed in R, with the corresponding scripts in the figures_and_analysis folder. All scripts can be run as is except for changing the `path` variables at the top. By default, the scripts will function so long as they are in the same directory as the corresponding result files.

The result files can be obtained by either downloading them from the 'intermediate results' folder in the Zenodo repository, or by running the Python scripts beforehand (the later may require moving the files or editing the `path` variables in each script to lead to the location of the Python scripts outputs). Copies of each script are also stored in this same folder of the repository and will work out-of-the-box, but may not be fully up to date compared to the scripts here.

- analysis_signatures.R is the script used for the acoustic distance GLMM-based analysis in Figure 1 and Table 2. The corresponding data is saved in dfw_euclidean_weight1_colony_distances.tsv and dfw_euclidean_weight1_source_distances.tsv. Note that this analysis uses different subsets of the data for the individual-level part of the analysis and for the other parts (nest-cluster-level, colony-level). The latter two use the whole dataset, while the individual-level part removes the calls from Unknown individual (which are exactly all the data from the 'Cambridge Wild' colony). This script also includes the code for Figure 1.

- analysis_knn.R is the script used for Figure 2, using the results from knn_stats.py, saved in dfw_euclidean_weight1_scoresFULL.tsv

- signatures_projection.R is the script used for creating the UMAP projections in Figure 3 of the article. It also holds the code snippets for tallying the calls used to create Table 1. The corresponding data is stored in dtw_euclidean_weight1_colony_embeddings.tsv.
