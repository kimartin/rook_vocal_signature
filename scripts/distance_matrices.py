from pickle import dump, HIGHEST_PROTOCOL, load
import numpy as np
import pandas as pd
import shutil
from datetime import datetime as dt

from functions.dtw import dtw_distance_matrix, euclidean, dtw, dfw, length_matrix, cosine, angular, normeuclidean
from functions.utilities import listfiles, tqdm2, flatten, product_dict
import config


if __name__ == '__main__':
    # Read spectrograms
    voc_files = listfiles(config.cluster_data, pattern='pickle', full_names=True)
    specs = [load(open(file, 'rb'))['final'].tolist() for file in tqdm2(voc_files)]
    specs = flatten(specs)
    specs = [np.ascontiguousarray((s.T / np.max(s)).astype(np.float64)) for s in specs]
    print(len(specs))

    # Prepare methods
    method_list = dict(method=[euclidean],
                       dtw_method=[dtw, dfw],
                       )

    # Folder to store the memmaps
    memmap_folder = 'C:/Users/killian/Desktop/temp'

    # Main loop
    method_list = product_dict(**method_list)
    for i, params in enumerate(method_list):
        print(dt.now())
        dtw_filename = f'{params["dtw_method"].__name__}_{params["method"].__name__}_weight1'
        dtw_filename = f'matrices/{dtw_filename}.pickle'
        dtw_dist = dtw_distance_matrix(specs,
                                       n_jobs=-1,
                                       batch_size=50000,
                                       pre_dispatch=48*50000,
                                       verbose=1,
                                       memmap_folder=memmap_folder,
                                       dtw_method_kwargs={'weight': 1.},
                                       **params,
                                       filename=None
                                       )

        length_dist = length_matrix(specs, type='sum', axis=0)
        with open(dtw_filename, 'wb') as con:
            dump(obj=dtw_dist / length_dist, file=con, protocol=HIGHEST_PROTOCOL)