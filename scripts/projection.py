import pickle
import pandas as pd
import umap, umap.plot as uplt
from sklearn.manifold import trustworthiness
import matplotlib.pyplot as plt
from math import ceil
from scipy.spatial.distance import squareform

from functions.utilities import flatten, tqdm2, listfiles
from functions.visualisations import scatter_projections

import config

if __name__ == '__main__':
    voc_files = listfiles(config.cluster_data, pattern='pickle', full_names=True)
    vocs = pd.concat([pickle.load(open(file, 'rb')) for file in voc_files])

    dtw_files = listfiles(f'matrices', pattern=['cid', 'pickle$'], negate=[True, False], full_names=True)

    df = []
    for _, dtw_file in enumerate(dtw_files):
        if 'dfw' in dtw_file:
            continue
        name = dtw_file.replace('matrices\\', '').replace('.pickle', '')
        dtw_matrix = pickle.load(open(dtw_file, 'rb'))
        dtw_matrix = squareform(dtw_matrix)

        fig, axs = plt.subplots(ncols=3, nrows=2)
        for i, n in enumerate([5, 15, 30]):
            embedding = umap.UMAP(n_neighbors=n,
                                  n_components=2,
                                  metric='precomputed',
                                  random_state=42,
                                  min_dist=0.).fit_transform(dtw_matrix)
            scatter_projections(
                embedding,
                labels=vocs['source'],
                show_legend=False,
                ax=axs[0, i],
                s=1,
                alpha=.5,
                color_palette="nipy_spectral"
            )
            scatter_projections(
                embedding,
                s=1,
                alpha=.5,
                labels=vocs['colony'],
                show_legend=True,
                ax=axs[1, i],
            )
        plt.tight_layout()
        plt.show()







