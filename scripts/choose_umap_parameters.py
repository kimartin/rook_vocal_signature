"""
Simply run UMAP many times and compute trustworthiness on the resulting embeddings.
"""

import pandas as pd
from datetime import datetime
import joblib
import pickle
from scipy.spatial.distance import squareform

from functions.utilities import listfiles, tqdm2, flatten
from functions.clusterability import multiple_embeddings, continuity_trustworthiness
import config

#Paths
voc_path = config.cluster_data
dtw_path = 'matrices'
out_path = 'embeddings'
if config.cluster_data is not None:
    dtw_path = f'{config.cluster_data}/{dtw_path}'
    out_path = f'{config.cluster_data}/{out_path}'

# Read in the data
voc_files = listfiles(voc_path, pattern='pickle', full_names=True)
vocs = pd.concat([joblib.load(open(file, 'rb')) for file in tqdm2(voc_files)], ignore_index=True)

# Distance matrices
dtw_files = listfiles(path=dtw_path, pattern=['cid', 'length'], negate=True)
memory_path = 'C:/Users/killian/Desktop/temp'

# Parameters to test, as a dict
umap_parameters = {
    "n_neighbors": [5, 15, 20, 30, 50, 100],
    "n_components": [2, 3, 5, 7, 10, 12, 15],
    'kind': ['PaCMAP', 'UMAP']
}

for i, dtw_file in enumerate(dtw_files):
    if '_euclidean' not in dtw_file:
        continue
    print(dtw_file)
    in_file = f'{dtw_path}/{dtw_file}'
    out_file = f'{out_path}/{dtw_file}'
    filename = dtw_file[:dtw_file.index('.')].replace(in_file, out_file)
    print(in_file, out_file, filename)
    raise ValueError

    dtw_dist = squareform(joblib.load(open(in_file, 'rb')))

    print('Starting embedding...', datetime.now())
    embeddings = multiple_embeddings(X=dtw_dist,
                                     test_parameters=umap_parameters,
                                     min_dist=0.,
                                     metric='precomputed',
                                     recycle_knn=False,
                                     suppress_warn=True,
                                     random_state=42)

    print('Computing metrics...', datetime.now())
    trust, cont = continuity_trustworthiness(X=dtw_dist,
                                             X_embedded=embeddings['embedding'],
                                             n_neighbors=embeddings['n_neighbors'],
                                             input_metric='precomputed',
                                             output_metric='euclidean')

    embeddings['trustworthiness'] = trust
    embeddings['continuity'] = cont
    embeddings['fidelity'] = 2 / (1 / trust + 1 / cont)
    print('Done', datetime.now())

    filename = dtw_file[:dtw_file.index('.')].replace(in_file, out_file)
    with open(f'{filename}_embedding.pickle', 'wb') as con:
        pickle.dump(obj=embeddings, file=con)
    embeddings.pop('embedding')
    pd.DataFrame(embeddings).to_csv(f'{filename}_embedding.tsv', sep='\t', index=False)