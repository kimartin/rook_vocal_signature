"""
This script takes pickle files output from dataset.py and computes spectrograms for each vocalisation.
Output:
    - one pickle file per input file, containing the original information, with the addition of the spectrogram data
"""
"""
Script to gather information on the vocs (saturation, SNR), then reduce noise
"""
import os
import pandas as pd
import numpy as np
import librosa
from functools import partial
import pickle
from joblib import Parallel, delayed
from os import cpu_count

import config
from functions.utilities import norm, compose, tqdm_joblib, env, np_dB, listfiles, restrict_kwargs
from functions.wav_manipulation import get_stft, norm_audio, filters, noise_reduction, center, segment
from functions.visualisations import plot_and_play_channels


# Helper function
def weighted_mean(x, axis=None):
    with np.errstate(divide='ignore', invalid='ignore'):
        weights = x / np.sum(x, axis=(1, 2), keepdims=axis is not None)
        return np.sum(x * weights, axis=axis) / np.sum(weights, axis=axis)


def circular_mean(angle, magnitude, axis=None):
    weights = magnitude / np.sum(magnitude, axis=axis, keepdims=axis is not None)
    sins = np.sin(angle)
    coss = np.cos(angle)
    return np.arctan2(np.sum(sins * weights, axis=axis) / np.sum(weights, axis=axis),
                      np.sum(coss * weights, axis=axis) / np.sum(weights, axis=axis))


# Filter channels with poor quality
def channel_candidates(saturation,
                       snr_gain=None,
                       snr=None,
                       measure=None,
                       tolerance=config.saturation_tolerance,
                       min_snr=-np.inf,
                       min_snr_gain=-np.inf,
                       lower_snr_under_max=0.,
                       lower_measure_under_max=0.,
                       ):

    # Remove channels with too much saturation
    candidates = [i for i, cand in enumerate(saturation)
                  if cand[0] < tolerance[0] and cand[1] < tolerance[1]]

    # Remove channels with too low SNR and gain
    candidates = [cand for cand, sn, gain in zip(candidates, snr[candidates], snr_gain[candidates])
                  if gain > min_snr_gain and sn > min_snr]

    # Keep only channels sufficiently close to the best channel
    candidates = [cand for cand, sn in zip(candidates, snr[candidates])
                  if max(snr[candidates]) - sn <= lower_snr_under_max]
    if measure is not None:
        candidates = [cand for cand, me in zip(candidates, measure[candidates])
                      if max(measure[candidates]) - me <= lower_measure_under_max]

    return candidates


def preprocess(wav,
               mel_filter=None,
               transforms=None,
               db_range=20.,
               power=None,
               **kwargs):
    # if len(kwargs) > 0:
    #     print(kwargs)

    if transforms is not None:
        wav = transforms(wav)

    # STFT
    spec, phase = get_stft(wav, **restrict_kwargs(get_stft, config.arg_dict))

    # Mel-scaling
    if mel_filter is not None:
        spec = np.matmul(mel_filter, spec)

    spec = norm(spec,
                axis=(-2, -1),
                high=np.quantile(spec, axis=(-2, -1), q=.99, keepdims=True),
                low=np.quantile(spec, axis=(-2, -1), q=.01, keepdims=True))
    if db_range is not None:
        spec = np_dB(spec,
                     ref=np.max,
                     axis=(-2, -1),
                     dB_range=db_range)
    if power is not None:
        if db_range is not None:
            spec = 10**(spec*db_range/10)
        spec **= power
        spec = norm(spec, axis=(-2, -1))

    # Reduces the space needed for the spectrograms
    # spec = (spec * 255).astype('uint8')

    # Compute the vocal envelope
    envelope = env(spec, axis=-2, mean='arith')
    envelope = norm(envelope, axis=0)

    return wav, spec, envelope


if __name__ == '__main__':
    # Prepare Mel filter
    if 'n_mels' in config.arg_dict and config.arg_dict['n_mels'] is not None:
        filter = librosa.filters.mel(**restrict_kwargs(librosa.filters.mel, config.arg_dict))
    else:
        filter = None

    wav_col = 'wav'
    spec_col = 'spectrogram'
    env_col = 'envelope'
    path_col = 'filename'

    # Read data
    vocs_files = listfiles(config.data_path, pattern=[f"{config.data_format}$"], full_names=True)

    voc_ = []
    total = 0
    for file in vocs_files:
        voc = pd.read_pickle(file)

        transforms = compose(
            partial(center, axis=-1),
        )

        voc[path_col] = [f'{config.audio_path}/{_}' if config.audio_path not in _ else _ for _ in voc[path_col]]

        voc = pd.concat([
            noise_reduction(group,
                            **restrict_kwargs(noise_reduction, config.arg_dict),
                            wav_col=wav_col,
                            denoised_col=wav_col,
                            start_col='start',
                            end_col='end',
                            group_col='Sequence',
                            channel_col='channel',
                            path_col=path_col,
                            transforms=transforms,
                            return_noises=False,
                            return_snr=False,
                            n_jobs=-1,
                            )
            for _, group in voc.groupby(path_col)
        ], ignore_index=True)

        transforms = compose(
            partial(filters, pre=-1., dc=-.9),
            partial(norm_audio, axis=-1)
        )

        with tqdm_joblib(total=voc.shape[0], desc="Processing...", position=0, leave=True):
            wavs, specs, envs = zip(
                *Parallel(n_jobs=-1,
                          batch_size=max(1, voc.shape[0] // cpu_count()))(
                    delayed(preprocess)(
                        wav=wav,
                        mel_filter=filter,
                        transforms=transforms,
                        db_range=20,
                        power=None,
                    )
                    for wav in voc[wav_col])
            )

        voc[f'{wav_col}_processed'] = pd.Series(wavs, index=voc.index)
        voc[spec_col] = pd.Series(specs, index=voc.index)
        voc[env_col] = pd.Series(envs, index=voc.index)

        voc['onsets_offsets'] = [segment(env,
                                         threshold=.05,
                                         shortest_voc=0,
                                         min_to_keep=0.5,
                                         merge_vocs_separated_by=3,
                                         trim=0,
                                         smooth=0,
                                         )
                                 if env is not None else (None, None)
                                 for env in voc[env_col]]

        voc["final"] = [spec[:, on[0]:off[-1]]
                        if len(on) > 0
                           and (sat[0][0] < config.saturation_tolerance[0] and sat[0][1] < config.saturation_tolerance[1])
                           and (off[-1] - on[0]) / spec.shape[0] > .5
                        else None
                        for spec, (on, off), sat in zip(voc[spec_col],
                                                        voc['onsets_offsets'],
                                                        voc['saturation'])]

        voc = voc.loc[[_ is not None for _ in voc['final']]].reset_index(drop=True)

        voc_.append(voc)

        with open(file, 'wb') as f:
            pickle.dump(obj=voc,
                        file=f,
                        protocol=pickle.HIGHEST_PROTOCOL)

    voc_ = pd.concat(voc_, axis=0, ignore_index=True)
    voc_['wav_processed'] = [np.expand_dims(w, axis=0) for w in voc_['wav_processed']]

    plot_and_play_channels(voc_,
                           n=300,
                           audio_col='wav',
                           spec_col='spectrogram',
                           env_col='envelope',
                           channel_col=None,
                           segment_col='onsets_offsets',
                           annotation_cols=['filename', 'source', 'start', 'end', 'saturation', 'channel'] + [c for c in voc_.columns if 'snr' in c],
                           hideaxes=False,
                           )



