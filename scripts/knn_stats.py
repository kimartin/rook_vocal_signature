import os

import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
import pickle
import pandas as pd
import numpy as np
from joblib import load
from scipy.spatial.distance import squareform
from scipy.stats import mode
from umap import UMAP
from collections import Counter
from sklearn.metrics import normalized_mutual_info_score, adjusted_mutual_info_score, adjusted_rand_score, rand_score

from functions.clusterability import pacmap_fit_transform
from functions.utilities import listfiles, tqdm2, flatten
from functions.visualisations import scatter_projections
import config


def justify(a, invalid_val, axis, side):
    """
    Justify ndarray for the valid elements (that are not invalid_val).

    Parameters
    ----------
    A : ndarray
        Input array to be justified
    invalid_val : scalar
        invalid value
    axis : int
        Axis along which justification is to be made
    side : str
        Direction of justification. Must be 'front' or 'end'.
        So, with 'front', valid elements are pushed to the front and
        with 'end' valid elements are pushed to the end along specified axis.
    """

    if invalid_val is np.nan:
        if isinstance(a, pd.DataFrame):
            mask = ~a.isnull()
        else:
            mask = ~np.isnan(a)
    else:
        mask = a != invalid_val
    justified_mask = np.sort(mask, axis=axis)

    if side == 'front':
        justified_mask = np.flip(justified_mask, axis=axis)

    out = np.full_like(a, fill_value=invalid_val)
    if axis == -1 or axis == a.ndim - 1:
        out[justified_mask] = a[mask]
    else:
        def pushax(a):
            return np.moveaxis(a, axis, -1)
        pushax(out)[pushax(justified_mask)] = pushax(a)[pushax(mask)]
    return out


def knn_stats(X,
              labels,
              discarding_labels=None,
              k=5,
              metric='euclidean',
              simplified=False,
              invalid_value='NOTHING'):

    if not hasattr(k, '__len__'):
        k = [k]

    # Fit the kNN instance to obtain the nearest neighbours of each point in X
    knn = NearestNeighbors(n_neighbors=X.shape[0]-1, metric=metric).fit(X)
    _, neighbours = knn.kneighbors()
    labels = np.array(labels)
    # get the labels of the neighbours in the correct order
    neighbour_labels = labels[neighbours]

    # Unique label values and respective counts in the original labels
    label_types, label_counts = np.unique(labels, return_counts=True)

    # discarding_labels allows removing points with the same secondary labels from the neighbourhood
    # e.g.: if points have labels "Colony" and discarding_labels is "Source"
    # then points with the same 'Source' value within a given 'Colony' value are discarded when computing the statistics
    if discarding_labels is not None:
        discarding_labels = np.array(discarding_labels)
        disc = (discarding_labels[neighbours] == discarding_labels[:, np.newaxis])
        neighbour_labels[disc] = invalid_value
        neighbour_labels = justify(neighbour_labels, invalid_val=invalid_value, axis=1, side='front')

    neighbour_labels = np.split(neighbour_labels, indices_or_sections=k, axis=1)
    S_list = []
    if simplified:
        S = pd.DataFrame({'Label': label_types, 'Count': 0, 'Total': 0}, index=label_types)
        for neigh in neighbour_labels[:-1]:
            for val_label in label_types:
                nei_ = neigh[labels == val_label]

                # number of knn neighbors of val_label that are also labelled val_label
                count = (nei_ == val_label).sum()
                S.loc[val_label, 'Count'] += count

                # total number of knn neighbors, excluding invalid_value
                tot = (nei_ != invalid_value).sum()
                S.loc[val_label, 'Total'] += tot

            S_list.append(S.copy())
    else:
        S = pd.DataFrame(0, index=label_types, columns=label_types)
        S_total = pd.DataFrame(0, index=label_types, columns=label_types)
        for neigh in neighbour_labels[:-1]:
            for val_label in label_types:
                nei_ = neigh[labels == val_label].ravel(order='C')
                size = nei_.size
                nei_ = Counter(nei_)
                invalids = nei_.pop(invalid_value, 0)
                S_total.loc[val_label] += size - invalids
                for val_neighbour, count in nei_.items():
                    S.loc[val_label, val_neighbour] += count

            # reshape into a new dataframe to prepare for the append call
            S_append = pd.melt(S.reset_index(drop=False, names='Point_Label'),
                               id_vars='Point_Label',
                               var_name='Neighbour_Label',
                               value_name='Count')
            S_append = S_append.merge(
                pd.melt(S_total.reset_index(drop=False, names='Point_Label'),
                        id_vars='Point_Label',
                        var_name='Neighbour_Label',
                        value_name='Total'),
                copy=False,
                on=['Point_Label', 'Neighbour_Label']
            )

            S_list.append(S_append.copy())

    S_tab = pd.concat(dict(zip(k, S_list)), axis=0, ignore_index=False).reset_index(level=0, names='k')

    S_tab['Proportion'] = S_tab['Count'] / S_tab['Total']
    S_tab['Point_Frequency'] = S_tab['Label' if simplified else 'Point_Label'].map(dict(zip(label_types, label_counts)))
    S_tab['Neighbour_Frequency'] = S_tab['Label' if simplified else 'Neighbour_Label'].map(dict(zip(label_types, label_counts)))
    S_tab['Expected'] = S_tab['Neighbour_Frequency'] / labels.size
    S_tab['Normalised'] = S_tab['Proportion'] / S_tab['Expected']

    return S_tab


def knn_metrics(X,
                labels,
                k=5,
                metric='euclidean',
                discarding_labels=None,
                ):
    if not hasattr(k, '__len__'):
        k = [k]

    labels = np.array(labels)
    knn = NearestNeighbors(n_neighbors=X.shape[0] - 1, metric=metric).fit(X)
    neighbours = knn.kneighbors(X, n_neighbors=X.shape[0] - 1, return_distance=False)
    neighbour_labels = labels[neighbours]
    neighbour_labels = pd.DataFrame(neighbour_labels.T)

    if discarding_labels is not None:
        discarding_neighbours = pd.DataFrame(np.array(discarding_labels)[neighbours].T, columns=discarding_labels)
        discarding_neighbours = discarding_neighbours.replace({c: {c: np.nan} for c in discarding_labels})
        neighbour_labels.columns = discarding_labels
        neighbour_labels[discarding_neighbours.isnull()] = discarding_neighbours
        neighbour_labels = neighbour_labels.transform(lambda x: sorted(x, key=pd.isnull))

    res = []
    for k_ in k:
        predicted = neighbour_labels.iloc[:k_].mode(axis=0, dropna=True).values[0]
        df = pd.DataFrame({
            'k': k_,
            'Accuracy': np.sum(labels == predicted) / labels.size,
            # 'Rand': rand_score(labels, predicted),
            # 'NMI': normalized_mutual_info_score(labels, predicted),
            'AdjRand': adjusted_rand_score(labels, predicted),
            'AdjNMI': adjusted_mutual_info_score(labels, predicted)
        }, index=[0])
        res.append(df)
    return pd.concat(res, axis=0, ignore_index=True)


voc_path = config.cluster_data
# voc_path = '/Volumes/Samsung_T5/embeddings/signatures'
dtw_path = 'matrices'
out_path = 'embeddings'
if config.cluster_data is not None:
    dtw_path = f'{voc_path}/{dtw_path}'
    out_path = f'{voc_path}/{out_path}'


# Read in the data
voc_files = listfiles(voc_path, pattern='pickle', full_names=True)
vocs = pd.concat([load(open(file, 'rb')) for file in tqdm2(voc_files)], ignore_index=True)

vocs.loc[vocs['source'].eq('Inc'), 'subgroup'] = 'Inc'
vocs.loc[vocs['colony'].str.contains('CAPTIVE'), 'subgroup'] = vocs.loc[vocs['colony'].str.contains('CAPTIVE'), 'colony']

common = vocs['source'].map(vocs['source'].value_counts() >= 5) & (~(vocs['source'].eq('Inc') & ~vocs['colony'].str.contains('CAMBRIDGE')))
conditions = {
    'colony': vocs.loc[common].index,
    'source': vocs.loc[~vocs['source'].eq('Inc') & common].index,
    # subgroup is the same as source but we still use it for the discarding below
    'subgroup': vocs.loc[~vocs['subgroup'].eq('Inc') & common].index,
}

# kNN neighbour values
neighbour_values_list = np.array([1, 2, 5, 10, 15, 30, 50, 75, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 900, 950, 1000])

# Embeddings: first the original D(F)TW space, then UMAP, then PaCMAP
umap_args = dict(n_neighbors=15, n_components=2, metric='precomputed', min_dist=0., random_state=42)
pacmap_args = dict(n_neighbors=15, n_components=2, metric='precomputed', random_state=42)

# Number of bootstraps
n_bootstraps = 1000

dtw_files = listfiles(path=dtw_path, pattern=['cid', 'length'], negate=True)

vocs = {
    cond: vocs.loc[idx].reset_index(drop=True)
    for cond, idx in conditions.items()
}

for i, dtw_file in enumerate(dtw_files):
    if 'euclidean' not in dtw_file:
        continue

    in_file = f'{dtw_path}/{dtw_file}'
    dtw_dist = squareform(load(open(in_file, 'rb')))

    # Add the exact numbers of ground truth labels, along with every possible pair thereof (2nd line)
    neighbour_values = {cond: np.unique(vocs[cond][cond].value_counts()) for cond in conditions}
    # neighbour_values = {cond: np.concatenate([val, np.add.outer(val, val).ravel()]) for cond, val in neighbour_values.items()}

    # Add the arbitrary values defined in neighbour_values_list
    neighbour_values = {cond: [*val, *[n for n in neighbour_values_list
                                       if n < 10 or np.min(np.abs(n - val)) > 10]]
                        for cond, val in neighbour_values.items()}

    # Upper bound for nearest neighbours: the size of the largest group for each condition
    neighbour_values = {cond: np.unique([_ for _ in val if _ < min(vocs[cond][cond].shape[0], 2 * vocs[cond][cond].value_counts().max())])
                        for cond, val in neighbour_values.items()}

    embeddings = {
        'Original': {cond: dtw_dist[np.ix_(idx, idx)] for cond, idx in conditions.items()},
        'UMAP': {cond: UMAP(**umap_args, low_memory=False).fit_transform(dtw_dist[np.ix_(idx, idx)]) for cond, idx in conditions.items()},
        'PaCMAP': {cond: pacmap_fit_transform(dtw_dist[np.ix_(idx, idx)], **pacmap_args) for cond, idx in conditions.items()},
    }

    metrics = []
    for name, embed in embeddings.items():
        metric = 'precomputed' if name == 'Original' else 'euclidean'
        metrics.extend(
            [
                knn_metrics(
                    X=embed[cond],
                    metric=metric,
                    k=neighbour_values[cond],
                    labels=vocs[cond][cond]
                ).assign(Condition=cond, Embedding=name)
                for cond, idx in conditions.items()
            ]
        )
        # Additional computations with the colony with the intra-individual distances removed
        metrics.append(
            knn_metrics(X=embed['source'],
                        # embed['source'] has to be used to remove the Inc in Cambridge_Sauvage
                        labels=vocs['source']['colony'],
                        discarding_labels=vocs['source']['source'],
                        metric=metric,
                        k=neighbour_values['colony']).assign(Condition='colonyWithoutSource', Embedding=name)
        )
        # Same but with the subgroups
        metrics.append(
            knn_metrics(X=embed['source'],
                        labels=vocs['source']['subgroup'],
                        discarding_labels=vocs['source']['source'],
                        metric=metric,
                        k=neighbour_values['subgroup']).assign(Condition='subgroupWithoutSource', Embedding=name)
        )
        # Same
        metrics.append(
            knn_metrics(X=embed['subgroup'],
                        labels=vocs['subgroup']['colony'],
                        discarding_labels=vocs['subgroup']['subgroup'],
                        metric=metric,
                        k=neighbour_values['colony']).assign(Condition='colonyWithoutSubgroup', Embedding=name)
        )

    metrics = pd.concat(metrics, axis=0, ignore_index=True)
    out_file = f'{out_path}/{dtw_file}'.replace('.pickle', f'_metricsFULL.tsv')
    metrics.to_csv(out_file, sep='\t', index=False, mode='w')

    # scores = []
    # for name, embed in embeddings.items():
    #     metric = 'precomputed' if name == 'Original' else 'euclidean'
    #     scores.extend(
    #         [
    #             knn_stats(
    #                 X=embed[cond],
    #                 labels=vocs[cond][cond],
    #                 metric=metric,
    #                 k=neighbour_values[cond],
    #                 simplified=False,
    #             ).assign(Condition=cond, Embedding=name)
    #             for cond, idx in conditions.items()
    #         ]
    #     )
    #
    #     # Additional computations with the colony with the intra-individual distances removed
    #     scores.append(
    #         knn_stats(X=embed['source'],
    #                   # embed['source'] has to be used to remove the Inc in Cambridge_Sauvage
    #                   labels=vocs['source']['colony'],
    #                   discarding_labels=vocs['source']['source'],
    #                   metric=metric,
    #                   simplified=False,
    #                   k=neighbour_values['colony']).assign(Condition='colonyWithoutSource', Embedding=name)
    #     )
    #     # Same but with the subgroups
    #     scores.append(
    #         knn_stats(X=embed['source'],
    #                   labels=vocs['source']['subgroup'],
    #                   discarding_labels=vocs['source']['source'],
    #                   metric=metric,
    #                   simplified=False,
    #                   k=neighbour_values['subgroup']).assign(Condition='subgroupWithoutSource', Embedding=name)
    #     )
    #     # Same
    #     scores.append(
    #         knn_stats(X=embed['subgroup'],
    #                   labels=vocs['subgroup']['colony'],
    #                   discarding_labels=vocs['subgroup']['subgroup'],
    #                   metric=metric,
    #                   simplified=False,
    #                   k=neighbour_values['colony']).assign(Condition='colonyWithoutSubgroup', Embedding=name)
    #     )
    #
    # scores = pd.concat(scores, axis=0, ignore_index=True)
    # out_file = f'{out_path}/{dtw_file}'.replace('.pickle', f'_scoresFULL.tsv')
    # scores.to_csv(out_file, sep='\t', index=False, mode='w')
    #
    for _ in range(n_bootstraps):
        print(_)
        # bootstrap: sample 80% of all vocalisations
        bootstrap = {cond: v.sample(frac=.8) for cond, v in vocs.items()}

        # Add the exact numbers of ground truth labels, along with every possible pair thereof (2nd line)
        neighbour_values = {cond: np.unique(bootstrap[cond][cond].value_counts()) for cond in conditions}
        # neighbour_values = {cond: np.concatenate([val, np.add.outer(val, val).ravel()]) for cond, val in neighbour_values.items()}

        # Add the arbitrary values defined in neighbour_values_list
        neighbour_values = {cond: [*val, *[n for n in neighbour_values_list
                                           if n < 10 or np.min(np.abs(n - val)) > 10]]
                            for cond, val in neighbour_values.items()}

        # Upper bound for nearest neighbours: the size of the largest group for each condition
        neighbour_values = {cond: np.unique(
            [_ for _ in val if _ < min(bootstrap[cond][cond].shape[0], 2 * bootstrap[cond][cond].value_counts().max())])
                            for cond, val in neighbour_values.items()}

        embeddings = {
            'Original': {cond: dtw_dist[np.ix_(idx.index, idx.index)] for cond, idx in bootstrap.items()},
            'UMAP': {cond: UMAP(**umap_args).fit_transform(dtw_dist[np.ix_(idx.index, idx.index)]) for cond, idx in
                     bootstrap.items()},
            'PaCMAP': {cond: pacmap_fit_transform(dtw_dist[np.ix_(idx.index, idx.index)], **pacmap_args) for cond, idx in
                       bootstrap.items()},
        }

        metrics = []
        for name, embed in embeddings.items():
            metric = 'precomputed' if name == 'Original' else 'euclidean'
            metrics.extend(
                [
                    knn_metrics(
                        X=embed[cond],
                        metric=metric,
                        k=neighbour_values[cond],
                        labels=bootstrap[cond][cond]
                    ).assign(Condition=cond, Embedding=name)
                    for cond, idx in conditions.items()
                ]
            )

            # Additional computations with the colony with the intra-individual distances removed
            metrics.append(
                knn_metrics(X=embed['source'],
                            # embed['source'] has to be used to remove the Inc in Cambridge_Sauvage
                            labels=bootstrap['source']['colony'],
                            discarding_labels=bootstrap['source']['source'],
                            metric=metric,
                            k=neighbour_values['colony']).assign(Condition='colonyWithoutSource', Embedding=name)
            )
            # Same but with the subgroups
            metrics.append(
                knn_metrics(X=embed['source'],
                            labels=bootstrap['source']['subgroup'],
                            discarding_labels=bootstrap['source']['source'],
                            metric=metric,
                            k=neighbour_values['subgroup']).assign(Condition='subgroupWithoutSource', Embedding=name)
            )
            # Same
            metrics.append(
                knn_metrics(X=embed['subgroup'],
                            labels=bootstrap['subgroup']['colony'],
                            discarding_labels=bootstrap['subgroup']['subgroup'],
                            metric=metric,
                            k=neighbour_values['colony']).assign(Condition='colonyWithoutSubgroup', Embedding=name)
            )

        metrics = pd.concat(metrics, axis=0, ignore_index=True)
        out_file = f'{out_path}/{dtw_file}'.replace('.pickle', f'_metricsBOOTSTRAP.tsv')
        metrics.to_csv(out_file, sep='\t', index=False, mode='a', header=not os.path.exists(out_file))
    #
    #     scores = []
    #     for name, embed in embeddings.items():
    #         metric = 'precomputed' if name == 'Original' else 'euclidean'
    #         scores.extend(
    #             [
    #                 knn_stats(
    #                     X=embed[cond],
    #                     labels=bootstrap[cond][cond],
    #                     metric=metric,
    #                     k=neighbour_values[cond],
    #                     simplified=True,
    #                 ).assign(Condition=cond, Embedding=name)
    #                 for cond, idx in conditions.items()
    #             ]
    #         )
    #
    #         # Additional computations with the colony with the intra-individual distances removed
    #         scores.append(
    #             knn_stats(X=embed['source'],
    #                       # embed['source'] has to be used to remove the Inc in Cambridge_Sauvage
    #                       labels=bootstrap['source']['colony'],
    #                       discarding_labels=bootstrap['source']['source'],
    #                       metric=metric,
    #                       simplified=True,
    #                       k=neighbour_values['colony']).assign(Condition='colonyWithoutSource', Embedding=name)
    #         )
    #         # Same but with the subgroups
    #         scores.append(
    #             knn_stats(X=embed['source'],
    #                       labels=bootstrap['source']['subgroup'],
    #                       discarding_labels=bootstrap['source']['source'],
    #                       metric=metric,
    #                       simplified=True,
    #                       k=neighbour_values['subgroup']).assign(Condition='subgroupWithoutSource', Embedding=name)
    #         )
    #         # Same
    #         scores.append(
    #             knn_stats(X=embed['subgroup'],
    #                       labels=bootstrap['subgroup']['colony'],
    #                       discarding_labels=bootstrap['subgroup']['subgroup'],
    #                       metric=metric,
    #                       simplified=True,
    #                       k=neighbour_values['colony']).assign(Condition='colonyWithoutSubgroup', Embedding=name)
    #         )
    #
    #     scores = pd.concat(scores, axis=0, ignore_index=True).assign(bootstrap=_)
    #     out_file = f'{out_path}/{dtw_file}'.replace('.pickle', f'_scoresBOOTSTRAP.tsv')
    #     scores.to_csv(out_file, sep='\t', index=False, mode='a', header=not os.path.exists(out_file))

