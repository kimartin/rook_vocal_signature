"""
Simply run UMAP many times and compute trustworthiness on the resulting embeddings.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from umap import UMAP
from joblib import load
from scipy.spatial.distance import squareform

from functions.clusterability import pacmap_fit_transform
from functions.utilities import listfiles, tqdm2, extract_triu
# from functions.visualisations import radialtree
from functions.dtw import euclidean
import config

voc_path = config.cluster_data
dtw_path = 'matrices'
out_path = 'embeddings'
if config.cluster_data is not None:
    dtw_path = f'{config.cluster_data}/{dtw_path}'
    out_path = f'{config.cluster_data}/{out_path}'
memory_path = 'C:/Users/killian/Desktop/temp'


# Read in the data
voc_files = listfiles(voc_path, pattern='pickle', full_names=True)
vocs = pd.concat([load(open(file, 'rb')) for file in tqdm2(voc_files)], ignore_index=True)

vocs.loc[vocs['source'].eq('Inc'), 'subgroup'] = 'Inc'
vocs.loc[vocs['colony'].str.contains('CAPTIVE'), 'subgroup'] = vocs.loc[vocs['colony'].str.contains('CAPTIVE'), 'colony']

common = vocs['source'].map(vocs['source'].value_counts() >= 5) & (~(vocs['source'].eq('Inc') & ~vocs['colony'].str.contains('CAMBRIDGE')))
conditions = {
    'colony': vocs.loc[common].index,
    'source': vocs.loc[~vocs['source'].eq('Inc') & common].index,
    # subgroup is the same as source so we don't use it
    # 'subgroup': vocs.loc[~vocs['subgroup'].eq('Inc') & common].index,
}


dtw_files = listfiles(path=dtw_path, pattern=['cid', 'length'], negate=True)

for i, dtw_file in enumerate(dtw_files):
    print(dtw_file)
    in_file = f'{dtw_path}/{dtw_file}'
    out_file = f'{out_path}/{dtw_file}'

    dtw_dist = load(open(in_file, 'rb'))
    dtw_dist = squareform(dtw_dist)

    for cond, idx in conditions.items():
        dist_cond = pd.DataFrame(data=dtw_dist[np.ix_(idx, idx)])
        dist_cond = extract_triu(dist_cond, k=1, valuename='dist')

        umap_embedding = UMAP(n_neighbors=15, n_components=2, min_dist=0., metric='precomputed', random_state=42).fit_transform(dtw_dist[np.ix_(idx, idx)])
        umap_cond = pd.DataFrame(euclidean(umap_embedding, umap_embedding))
        umap_cond = extract_triu(umap_cond, k=1, valuename='umap')
        dist_cond = dist_cond.merge(umap_cond, how='inner', on=['Row', 'Column'])

        pacmap_embedding = pacmap_fit_transform(dtw_dist[np.ix_(idx, idx)], n_neighbors=15, n_components=2, random_state=42)
        pacmap_cond = pd.DataFrame(euclidean(pacmap_embedding, pacmap_embedding))
        pacmap_cond = extract_triu(pacmap_cond, k=1, valuename='pacmap')
        dist_cond = dist_cond.merge(pacmap_cond, how='inner', on=['Row', 'Column'])

        labels = vocs.iloc[idx].reset_index(drop=True)
        for key in ['colony', 'subgroup', 'source', 'file']:
            dist_cond[f'x_{key}_unsorted'] = labels[key].iloc[dist_cond['Row']].values
            dist_cond[f'y_{key}_unsorted'] = labels[key].iloc[dist_cond['Column']].values

            # this ensures that dyads of the same values but in different order are not considered as different
            arr = dist_cond[[f'x_{key}_unsorted', f'y_{key}_unsorted']].values
            arr.sort(axis=1)
            dist_cond[[f'x_{key}', f'y_{key}']] = arr

        filename = out_file[:out_file.index('.')]
        dist_cond.to_csv(f'{filename}_{cond}_distances.tsv', sep='\t', index=False)

        labels[['UMAP_V0', 'UMAP_V1']] = umap_embedding
        labels[['PACMAP_V0', 'PACMAP_V1']] = pacmap_embedding
        labels.to_csv(f'{filename}_{cond}_embeddings.tsv', sep='\t', index=False)