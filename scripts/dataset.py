"""
This script takes the label and audio files, and creates a database containing the vocalisations.

Using:
    - edit config.py with the correct paths to the audio files AND to the label files
    - Note: the label files should include a column giving the name of the audio file to sample, the start and end times
        in seconds, the channel to use, and any other information (not used in this script but for later

Output:
    - one pickle file per input label file, for further use in Python scripts
    - each pickle file contains the original label information, an additional 'wav' column containing the vocalisation
        data, and a 'samp.rate' column containing the sampling rate of the audio.
"""

import os
import pandas as pd
from joblib import delayed, Parallel
from pickle import dump

import config
from functions.utilities import find_overlaps, listfiles, tqdm2
from functions.wav_manipulation import detect_saturation, wav_read, batchsize

start_col = 'start'
end_col = 'end'
file_col = 'filename'
ignore_col = "source"
ignore_values = ['Pls', 'Ignore', 'Comment']
channel_col = 'channel'

# Get file names
names = listfiles(path=config.label_path, pattern=[f"{config.label_format}", "\\._"], negate=[False, True])
# audio = listfiles(path=config.audio_path, pattern=[f"{config.audio_format}", "\\._"], negate=[False, True])

for file in tqdm2(names):
    voc = pd.read_table(os.path.join(config.label_path, file) if config.label_path else file)

    # voc = voc.sort_values(by=start_col)
    # Note overlaps and voc sequences (independenly of Source)
    voc = find_overlaps(voc, start=start_col, end=end_col, separated_by=config.maximum_interval, by=file_col, name="Sequence")

    # Insert a column giving the audio file name for easier reading later
    voc[file_col] = [f'{config.audio_path}/{aud}' if config.audio_path else aud for aud in voc[file_col]]

    idx = voc.loc[~voc[ignore_col].isin(ignore_values)].index

    # Read in sound data
    with Parallel(n_jobs=-1,
                  batch_size=batchsize(n=voc.shape[0], n_jobs=-1),
                  pre_dispatch=2*os.cpu_count(),
                  verbose=1) as par:
        wav_data = par(delayed(wav_read)(file=file,
                                         start=start,
                                         end=end,
                                         units='seconds',
                                         sr=config.sample_rate,
                                         channel=ch,
                                         extra=config.wl)
                       for file, val, start, end, ch in
                       voc[[file_col, ignore_col, start_col, end_col, channel_col]].itertuples(index=False)
                       if val not in ignore_values
                       )
    wav_data = pd.DataFrame(dict(zip(['wav', 'samp.rate'], zip(*wav_data))), index=idx)
    voc = pd.concat([voc, wav_data], ignore_index=False, axis=1)

    with Parallel(n_jobs=1) as par:
        sat = par(delayed(detect_saturation)(x=w, bit=config.bit)
                  for w in voc.loc[~voc[ignore_col].isin(ignore_values), 'wav'])
        voc.loc[~voc[ignore_col].isin(ignore_values), 'saturation'] = pd.Series(sat, index=idx)

    # Pickle the final files
    out_file = f"{file.replace(f'.{config.label_format}', '')}_{config.voc_name}.pickle"
    with open(f'{config.data_path}/{out_file}' if config.data_path else out_file, 'wb') as voc_file:
        dump(obj=voc, file=voc_file)

